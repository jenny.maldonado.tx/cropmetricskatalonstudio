package commandLine

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

public class cmd {

	@Keyword
	def cmdLine(String command, String password) {

		Runtime rtDb = Runtime.getRuntime();

		ProcessBuilder pbuilderDb = new ProcessBuilder("cmd.exe", "/c", command);

		//Add the password of the database as environment variable of postgres
		pbuilderDb.environment().put( "PGPASSWORD", password);
		pbuilderDb.redirectErrorStream( true );

		//Run the process
		Process pDb = pbuilderDb.start();
		Thread.sleep(5000);

		/*
		 // To print line by line the response in the cmd window
		 BufferedReader r = new BufferedReader(new InputStreamReader(p.getInputStream()));
		 while (true) {
		 String line = r.readLine();
		 if (line == null) {break;}
		 System.out.println(line);
		 }
		 */
	}

	@Keyword
	def singleCommand(String command) {

		Runtime r = Runtime.getRuntime();
		ProcessBuilder pbuilder = new ProcessBuilder("cmd.exe", "/c", command);
		pbuilder.redirectErrorStream( true );
		Process p = pbuilder.start();
		Thread.sleep(1000);
	}
}
