<?xml version="1.0" encoding="UTF-8"?>
<WebServiceRequestEntity>
   <description></description>
   <name>RainBucketReadings</name>
   <tag></tag>
   <elementGuidId>4bb866ab-50bd-4d4c-bf7f-4b50be12be12</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <followRedirects>false</followRedirects>
   <httpBody></httpBody>
   <httpBodyContent>{
  &quot;text&quot;: &quot;{\n  \&quot;properties\&quot;:{},\n  \&quot;routing_key\&quot;:\&quot;\&quot;,\n  \&quot;payload\&quot;:\&quot;{ \\\&quot;messageType\\\&quot;: [     \\\&quot;urn:message:CropMetrics.Probes.Ingest.Messages.Contracts.Interfaces:IRequestRainBucketReadingsUpdatePerProbeId\\\&quot;   ],   \\\&quot;message\\\&quot;: {     \\\&quot;probeId\\\&quot;: 37,     \\\&quot;dataServiceId\\\&quot;: 1,     \\\&quot;messageDateTime\\\&quot;: \\\&quot;2018-12-17T19:34:06.3329801Z\\\&quot;,     \\\&quot;guid\\\&quot;: \\\&quot;e80fffa2-b3e3-4280-9844-a0fa56878013\\\&quot;   } }\&quot;,\n  \&quot;payload_encoding\&quot;:\&quot;string\&quot;\n}\n&quot;,
  &quot;contentType&quot;: &quot;application/json&quot;,
  &quot;charset&quot;: &quot;UTF-8&quot;
}</httpBodyContent>
   <httpBodyType>text</httpBodyType>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Content-Type</name>
      <type>Main</type>
      <value>application/json</value>
   </httpHeaderProperties>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Authorization</name>
      <type>Main</type>
      <value>${authorization}</value>
   </httpHeaderProperties>
   <migratedVersion>5.4.1</migratedVersion>
   <restRequestMethod>POST</restRequestMethod>
   <restUrl>http://localhost:15672/api/exchanges/%2F/CropMetricsProbesIngestJsonReadings_RequestRainBucketReadingsUpdatePerProbeId/publish</restUrl>
   <serviceType>RESTful</serviceType>
   <soapBody></soapBody>
   <soapHeader></soapHeader>
   <soapRequestMethod></soapRequestMethod>
   <soapServiceFunction></soapServiceFunction>
   <variables>
      <defaultValue>'Basic Z3Vlc3Q6Z3Vlc3Q='</defaultValue>
      <description></description>
      <id>a7e40641-03d9-451c-974d-89c61a5064c2</id>
      <masked>false</masked>
      <name>authorization</name>
   </variables>
   <verificationScript>import static org.assertj.core.api.Assertions.*

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webservice.verification.WSResponseManager

import groovy.json.JsonSlurper
import internal.GlobalVariable as GlobalVariable

RequestObject request = WSResponseManager.getInstance().getCurrentRequest()

ResponseObject response = WSResponseManager.getInstance().getCurrentResponse()

WS.verifyResponseStatusCode(response, 200)

assertThat(response.getStatusCode()).isEqualTo(200)</verificationScript>
   <wsdlAddress></wsdlAddress>
</WebServiceRequestEntity>
