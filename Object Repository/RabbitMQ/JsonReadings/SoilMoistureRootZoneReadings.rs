<?xml version="1.0" encoding="UTF-8"?>
<WebServiceRequestEntity>
   <description></description>
   <name>SoilMoistureRootZoneReadings</name>
   <tag></tag>
   <elementGuidId>2a660e12-875c-4a59-bbf1-1985e6378880</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <followRedirects>false</followRedirects>
   <httpBody></httpBody>
   <httpBodyContent>{
  &quot;text&quot;: &quot;{\n  \&quot;properties\&quot;:{},\n  \&quot;routing_key\&quot;:\&quot;\&quot;,\n  \&quot;payload\&quot;:\&quot;{ \\\&quot;messageType\\\&quot;: [\\\&quot;urn:message:CropMetrics.Probes.Ingest.Messages.Contracts.Interfaces:IRequestSoilMoistureRootZoneReadingsUpdatePerProbeId\\\&quot;],\\\&quot;message\\\&quot;: {\\\&quot;probeId\\\&quot;: 37,       \\\&quot;dataServiceId\\\&quot;: 1,       \\\&quot;messageDateTime\\\&quot;: \\\&quot;2019-04-15T18:59:44.2571727Z\\\&quot;,       \\\&quot;guid\\\&quot;: \\\&quot;c6b89531-6c9e-412c-986e-fac60a117a8d\\\&quot;     } }\&quot;,\n  \&quot;payload_encoding\&quot;:\&quot;string\&quot;\n}&quot;,
  &quot;contentType&quot;: &quot;application/json&quot;,
  &quot;charset&quot;: &quot;UTF-8&quot;
}</httpBodyContent>
   <httpBodyType>text</httpBodyType>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Content-Type</name>
      <type>Main</type>
      <value>application/json</value>
   </httpHeaderProperties>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Authorization</name>
      <type>Main</type>
      <value>${authorization}</value>
   </httpHeaderProperties>
   <migratedVersion>5.4.1</migratedVersion>
   <restRequestMethod>POST</restRequestMethod>
   <restUrl>http://localhost:15672/api/exchanges/%2F/CropMetricsProbesIngestJsonReadings_RequestSoilMoistureRootZoneReadingsUpdatePerProbeId/publish</restUrl>
   <serviceType>RESTful</serviceType>
   <soapBody></soapBody>
   <soapHeader></soapHeader>
   <soapRequestMethod></soapRequestMethod>
   <soapServiceFunction></soapServiceFunction>
   <variables>
      <defaultValue>'Basic Z3Vlc3Q6Z3Vlc3Q='</defaultValue>
      <description></description>
      <id>28455a1e-79be-4084-a2df-b0ee070b1e14</id>
      <masked>false</masked>
      <name>authorization</name>
   </variables>
   <verificationScript>import static org.assertj.core.api.Assertions.*

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webservice.verification.WSResponseManager

import groovy.json.JsonSlurper
import internal.GlobalVariable as GlobalVariable

RequestObject request = WSResponseManager.getInstance().getCurrentRequest()

ResponseObject response = WSResponseManager.getInstance().getCurrentResponse()

WS.verifyResponseStatusCode(response, 200)

assertThat(response.getStatusCode()).isEqualTo(200)</verificationScript>
   <wsdlAddress></wsdlAddress>
</WebServiceRequestEntity>
