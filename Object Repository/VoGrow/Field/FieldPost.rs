<?xml version="1.0" encoding="UTF-8"?>
<WebServiceRequestEntity>
   <description></description>
   <name>FieldPost</name>
   <tag></tag>
   <elementGuidId>da275241-abae-48c9-b044-46c8d15c5306</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <followRedirects>false</followRedirects>
   <httpBody></httpBody>
   <httpBodyContent>{
  &quot;text&quot;: &quot;{\n  \&quot;field\&quot;: {\n    \&quot;farm_id\&quot;: 2,\n    \&quot;name\&quot;: \&quot;fld katalon11\&quot;,\n    \&quot;boundary\&quot;: {\n      \&quot;boundaries\&quot;: [\n        {\n          \&quot;boundary\&quot;: {\n            \&quot;coords\&quot;: [\n              {\n                \&quot;x\&quot;: -101.49568751,\n                \&quot;y\&quot;: 37.14976293\n              },\n              {\n                \&quot;x\&quot;: -101.50049403,\n                \&quot;y\&quot;: 37.14647907\n              },\n              {\n                \&quot;x\&quot;: -101.50049403,\n                \&quot;y\&quot;: 37.14264773\n              },\n              {\n                \&quot;x\&quot;: -101.49723246,\n                \&quot;y\&quot;: 37.1411425\n              },\n              {\n                \&quot;x\&quot;: -101.49019435,\n                \&quot;y\&quot;: 37.14223722\n              },\n              {\n                \&quot;x\&quot;: -101.48933604,\n                \&quot;y\&quot;: 37.14620541\n              },\n              {\n                \&quot;x\&quot;: -101.49294093,\n                \&quot;y\&quot;: 37.14866833\n              },\n              {\n                \&quot;x\&quot;: -101.49568751,\n                \&quot;y\&quot;: 37.14976293\n              }\n            ]\n          }\n        }\n      ]\n    }\n  }\n}&quot;,
  &quot;contentType&quot;: &quot;application/json&quot;,
  &quot;charset&quot;: &quot;UTF-8&quot;
}</httpBodyContent>
   <httpBodyType>text</httpBodyType>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Content-Type</name>
      <type>Main</type>
      <value>application/json</value>
   </httpHeaderProperties>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Authorization</name>
      <type>Main</type>
      <value>Bearer ${access_token}</value>
   </httpHeaderProperties>
   <migratedVersion>5.4.1</migratedVersion>
   <restRequestMethod>POST</restRequestMethod>
   <restUrl>http://localhost:10521/WebAPI/api/mobile/v1/fields</restUrl>
   <serviceType>RESTful</serviceType>
   <soapBody></soapBody>
   <soapHeader></soapHeader>
   <soapRequestMethod></soapRequestMethod>
   <soapServiceFunction></soapServiceFunction>
   <variables>
      <defaultValue>'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1bmlxdWVfbmFtZSI6Implbm55Lm1hbGRvbmFkby50eDFAZ21haWwuY29tIiwicm9sZSI6InVzZXIiLCJzdWIiOiJqZW5ueS5tYWxkb25hZG8udHgxQGdtYWlsLmNvbSIsImlzcyI6IkNyb3BNZXRyaWNzIiwiYXVkIjoiYWxsIiwiZXhwIjoxNTU0NDYzNTkzLCJuYmYiOjE1NTQ0NTk2OTN9.z60ggtftp_qBR0kmT91lJZQXPEaOJB0W5ZZz8OPMWeA'</defaultValue>
      <description></description>
      <id>ccc4d5b0-16ae-45a7-a3c3-284e27bc9da0</id>
      <masked>false</masked>
      <name>access_token</name>
   </variables>
   <verificationScript>import static org.assertj.core.api.Assertions.*

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webservice.verification.WSResponseManager

import groovy.json.JsonSlurper
import internal.GlobalVariable as GlobalVariable

RequestObject request = WSResponseManager.getInstance().getCurrentRequest()

ResponseObject response = WSResponseManager.getInstance().getCurrentResponse()



WS.verifyResponseStatusCode(response, 201)

assertThat(response.getStatusCode()).isEqualTo(201)</verificationScript>
   <wsdlAddress></wsdlAddress>
</WebServiceRequestEntity>
