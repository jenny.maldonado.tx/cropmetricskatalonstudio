<?xml version="1.0" encoding="UTF-8"?>
<WebServiceRequestEntity>
   <description></description>
   <name>SeasonSummaryReport</name>
   <tag></tag>
   <elementGuidId>13fb9b44-aa61-4fa7-a74d-17890524c0dc</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <followRedirects>false</followRedirects>
   <httpBody></httpBody>
   <httpBodyContent></httpBodyContent>
   <httpBodyType></httpBodyType>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Authorization</name>
      <type>Main</type>
      <value>Bearer ${access_token}</value>
   </httpHeaderProperties>
   <migratedVersion>5.4.1</migratedVersion>
   <restRequestMethod>GET</restRequestMethod>
   <restUrl>http://localhost:10520/WebAPI/api/mobile/v1/fields/${field_id}/season-report</restUrl>
   <serviceType>RESTful</serviceType>
   <soapBody></soapBody>
   <soapHeader></soapHeader>
   <soapRequestMethod></soapRequestMethod>
   <soapServiceFunction></soapServiceFunction>
   <variables>
      <defaultValue>'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1bmlxdWVfbmFtZSI6ImNyb3BtZXRyaWNzLmFnZW50cndAZ21haWwuY29tIiwicm9sZSI6InVzZXIiLCJzdWIiOiJjcm9wbWV0cmljcy5hZ2VudHJ3QGdtYWlsLmNvbSIsImlzcyI6IkNyb3BNZXRyaWNzIiwiYXVkIjoiYWxsIiwiZXhwIjoxNTcwNzIzMTc3LCJuYmYiOjE1NjgxMzA4Nzd9.49iPnRnCN6lx8Y-Pijs_g8KwpKEmwU6HNOdfu91hBFk'</defaultValue>
      <description></description>
      <id>4e0dd827-2e37-4c67-8e74-0f8d27de85d2</id>
      <masked>false</masked>
      <name>access_token</name>
   </variables>
   <variables>
      <defaultValue>1</defaultValue>
      <description></description>
      <id>c93ba987-4b1d-4c4a-81ae-8a1bf888d1fd</id>
      <masked>false</masked>
      <name>field_id</name>
   </variables>
   <verificationScript>import static org.assertj.core.api.Assertions.*

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webservice.verification.WSResponseManager

import groovy.json.JsonSlurper
import internal.GlobalVariable as GlobalVariable

RequestObject request = WSResponseManager.getInstance().getCurrentRequest()

ResponseObject response = WSResponseManager.getInstance().getCurrentResponse()


WS.verifyResponseStatusCode(response, 200)

assertThat(response.getStatusCode()).isEqualTo(200)</verificationScript>
   <wsdlAddress></wsdlAddress>
</WebServiceRequestEntity>
