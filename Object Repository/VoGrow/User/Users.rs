<?xml version="1.0" encoding="UTF-8"?>
<WebServiceRequestEntity>
   <description></description>
   <name>Users</name>
   <tag></tag>
   <elementGuidId>76b25f42-73d2-42b7-9d68-b7cef1e80df7</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <httpBody></httpBody>
   <httpBodyContent>{
  &quot;text&quot;: &quot;{\n  \&quot;field\&quot;: {\n    \&quot;farm_id\&quot;: 2,\n    \&quot;name\&quot;: \&quot;fld katalon11\&quot;,\n    \&quot;boundary\&quot;: {\n      \&quot;boundaries\&quot;: [\n        {\n          \&quot;boundary\&quot;: {\n            \&quot;coords\&quot;: [\n              {\n                \&quot;x\&quot;: -101.49568751,\n                \&quot;y\&quot;: 37.14976293\n              },\n              {\n                \&quot;x\&quot;: -101.50049403,\n                \&quot;y\&quot;: 37.14647907\n              },\n              {\n                \&quot;x\&quot;: -101.50049403,\n                \&quot;y\&quot;: 37.14264773\n              },\n              {\n                \&quot;x\&quot;: -101.49723246,\n                \&quot;y\&quot;: 37.1411425\n              },\n              {\n                \&quot;x\&quot;: -101.49019435,\n                \&quot;y\&quot;: 37.14223722\n              },\n              {\n                \&quot;x\&quot;: -101.48933604,\n                \&quot;y\&quot;: 37.14620541\n              },\n              {\n                \&quot;x\&quot;: -101.49294093,\n                \&quot;y\&quot;: 37.14866833\n              },\n              {\n                \&quot;x\&quot;: -101.49568751,\n                \&quot;y\&quot;: 37.14976293\n              }\n            ]\n          }\n        }\n      ]\n    }\n  }\n}&quot;,
  &quot;contentType&quot;: &quot;application/json&quot;,
  &quot;charset&quot;: &quot;UTF-8&quot;
}</httpBodyContent>
   <httpBodyType>text</httpBodyType>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Content-Type</name>
      <type>Main</type>
      <value>application/json</value>
   </httpHeaderProperties>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Authorization</name>
      <type>Main</type>
      <value>Bearer ${access_token}</value>
   </httpHeaderProperties>
   <migratedVersion>5.4.1</migratedVersion>
   <restRequestMethod>GET</restRequestMethod>
   <restUrl>http://localhost:10520/WebAPI/api/mobile/v1/users?</restUrl>
   <serviceType>RESTful</serviceType>
   <soapBody></soapBody>
   <soapHeader></soapHeader>
   <soapRequestMethod></soapRequestMethod>
   <soapServiceFunction></soapServiceFunction>
   <variables>
      <defaultValue>'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1bmlxdWVfbmFtZSI6Implbm55Lm1hbGRvbmFkby50eC5hZ2VudC5yb0BnbWFpbC5jb20iLCJyb2xlIjoidXNlciIsInN1YiI6Implbm55Lm1hbGRvbmFkby50eC5hZ2VudC5yb0BnbWFpbC5jb20iLCJpc3MiOiJDcm9wTWV0cmljcyIsImF1ZCI6ImFsbCIsImV4cCI6MTU1NDgzMjEzNywibmJmIjoxNTU0ODI4MjM3fQ.rq475YMlN11LQ3NpWZ8JPUlXoRzjn-N0zvzVo69z_aU'</defaultValue>
      <description></description>
      <id>ccc4d5b0-16ae-45a7-a3c3-284e27bc9da0</id>
      <masked>false</masked>
      <name>access_token</name>
   </variables>
   <verificationScript>import static org.assertj.core.api.Assertions.*

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webservice.verification.WSResponseManager

import groovy.json.JsonSlurper
import internal.GlobalVariable as GlobalVariable

RequestObject request = WSResponseManager.getInstance().getCurrentRequest()

ResponseObject response = WSResponseManager.getInstance().getCurrentResponse()



WS.verifyResponseStatusCode(response, 200)

assertThat(response.getStatusCode()).isEqualTo(200)</verificationScript>
   <wsdlAddress></wsdlAddress>
</WebServiceRequestEntity>
