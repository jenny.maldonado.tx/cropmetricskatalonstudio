import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable


// connection DB
// Use the next ssurgo endpoint, take note the table data
// URI: https://SDMDataAccess.sc.egov.usda.gov/Tabular/post.rest
// take note to use the ssrugo endpoint for eacho mukey stored, all data is saved as txt files to the automation

def FieldId = '12' // to be changed

// Connection to vo_fieldsProcessed databaase
CustomKeywords.'connectionCmDatabase.CmConnection.connectDB'('localhost', 'vo_fieldsProcessed', '5432', 'postgres', 'admin')

// Variables that contain the ssurgo txt files for each mukey of the FieldId
def mukey1 = 1444759
def mukeySsurgo1 = new File("C:\\KatalonFiles\\Fld12_1444759.txt").getText('UTF-8').replaceAll(" ","").replaceAll("\r\n", "");

def mukey2 = 1444770
def mukeySsurgo2 = new File("C:\\KatalonFiles\\Fld12_1444770.txt").getText('UTF-8').replaceAll(" ","").replaceAll("\r\n", "");

def mukey3 = 1444802
def mukeySsurgo3 = new File("C:\\KatalonFiles\\Fld12_1444802.txt").getText('UTF-8').replaceAll(" ","").replaceAll("\r\n", "");

def mukey4 = 1444820
def mukeySsurgo4 = new File("C:\\KatalonFiles\\Fld12_1444820.txt").getText('UTF-8').replaceAll(" ","").replaceAll("\r\n", "");

// Print new records created in vo_fieldsProcessed db
def query = "select mukey, soiltexture from spatialdata.zones where field_id = " + FieldId + "order by mukey asc"
def queryResult = CustomKeywords.'connectionCmDatabase.CmConnection.executeQuery'(query)

while (queryResult.next()) {
	mukey = queryResult.getString(1)
	soiltexture = queryResult.getString(2).replaceAll(" ","").replaceAll("\r\n", "");
	
	switch(mukey)
	{
		case "1444759":
				WS.verifyEqual(mukey,mukey1)
				WS.verifyEqual(mukeySsurgo1,soiltexture)
				println ('Mukey '+ mukey +' from ssurgo is     :' + mukeySsurgo1)
				println ('Ssurgo for mukey '+ mukey +' in DB is:' + soiltexture)
				break;
				
		case "1444770":
				WS.verifyEqual(mukey,mukey2)
				WS.verifyEqual(mukeySsurgo2,soiltexture)
				println ('Mukey '+ mukey +' from ssurgo is     :' + mukeySsurgo2)
				println ('Ssurgo for mukey '+ mukey +' in DB is:' + soiltexture)
				break;
				
		case "1444802":
				WS.verifyEqual(mukey,mukey3)
				WS.verifyEqual(mukeySsurgo3,soiltexture)
				println ('Mukey '+ mukey +' from ssurgo is     :' + mukeySsurgo3)
				println ('Ssurgo for mukey '+ mukey +' in DB is:' + soiltexture)
				break;
				
		case "1444820":
				WS.verifyEqual(mukey,mukey4)
				WS.verifyEqual(mukeySsurgo4,soiltexture)
				println ('Mukey '+ mukey +' from ssurgo is     :' + mukeySsurgo4)
				println ('Ssurgo for mukey '+ mukey +' in DB is:' + soiltexture)
				break;

	}	
}

CustomKeywords.'connectionCmDatabase.CmConnection.closeDatabaseConnection'()