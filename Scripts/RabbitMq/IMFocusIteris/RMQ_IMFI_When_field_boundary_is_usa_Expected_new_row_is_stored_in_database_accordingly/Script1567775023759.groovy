import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable


// connection DB
// Use the next iteris endpoint, take note the planting date of the growth in the field to start with start_time
// Irrigation Conditions with Custom Analysis Daily - v1.0:  
// https://ag.us.clearapis.com/v1.0/crop_health/irrigation/custom_analysis/daily?app_id={{APP_ID}}&app_key={{APP_KEY}}&account_id={{ACCOUNT_ID}}&user_id={{user_id}}&growth_id={{growth_id}}&start_date=2019-04-18&end_date=2019-09-05&unitcode={{unitcode}}

def FieldId = '1' // to be changed

// Connection to iterisRawData databaase
CustomKeywords.'connectionCmDatabase.CmConnection.connectDB'('localhost', 'iterisRawData', '5432', 'postgres', 'admin')

// Print new record created in iterisRawData db
def query = 'select data, metadata from imfocus.uniformirrigationdailyconditions where field_id = ' + FieldId
def queryResult = CustomKeywords.'connectionCmDatabase.CmConnection.executeQuery'(query)

while (queryResult.next()) {
	data = queryResult.getString(1);
	metadata = queryResult.getString(2);
	
	if(data != null)
	{
		println('Data for daily condition for field id ' + FieldId + ' is: ' + data);
		println('Metadata for daily condition for field id' + FieldId + ' is: ' + metadata);
	}
	else
	{
		assert(data.isEmpty())
	}
}

CustomKeywords.'connectionCmDatabase.CmConnection.closeDatabaseConnection'()

