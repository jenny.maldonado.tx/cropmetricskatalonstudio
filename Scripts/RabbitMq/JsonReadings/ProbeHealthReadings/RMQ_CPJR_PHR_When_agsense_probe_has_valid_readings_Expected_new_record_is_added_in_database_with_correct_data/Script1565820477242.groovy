import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable


// Take note of the number of the decimals for the battery and signalStrength , should be 2 and 1 consecutively
// serial probe = 79810 , with only 1 valid reading in the current day

def probeSerial = '79810'
def probeId = '37' // to be changed
 
// Connection to vo_agSenseProbeRawReadings dtabaase
CustomKeywords.'connectionCmDatabase.CmConnection.connectDB'('localhost', 'vo_agSenseProbeRawReadings', '5432', 'postgres', 'admin')

// Print the values from the vo_agSenseProbeRawReadings database
def query = "select date(readingdatetime),to_char(readingdatetime,'HH24:MI:SS.US'), power, round(battery::numeric,2), round(signalStrength::numeric,1), date(telemetryDateTime),to_char(telemetryDateTime,'HH24:MI:SS.US') from agsenseprobereading where serial = '" + probeSerial +"'"
def queryResult = CustomKeywords.'connectionCmDatabase.CmConnection.executeQuery'(query)

while (queryResult.next())
{
	def readingDate= queryResult.getString(1)
	def readingTime= queryResult.getString(2)
	def power = queryResult.getString(3)
	def battery = queryResult.getString(4)
	def signalStrength = queryResult.getString(5)
	def telemetryDate = queryResult.getString(6)
	def telemetryTime = queryResult.getString(7)
		
	println('The readingDateTime is :' + readingDate + 'T' + readingTime)
	println('The power is :' + power)
	println('The battery is :' + battery)
	println('The signalStrength is :' + signalStrength)
	println('The telemetryDateTime is :' + telemetryDate + 'T' + telemetryTime)
	
	GlobalVariable.AgSenseReadings = '[{"power": "' + power + '", "battery": ' + battery + ', "signalStrength": ' + signalStrength + ', "readingDateTime": "' + readingDate + 'T' + readingTime + '", "telemetryDateTime": "' + telemetryDate + 'T' + telemetryTime + '"}]'
	
}

println(GlobalVariable.AgSenseReadings)

CustomKeywords.'connectionCmDatabase.CmConnection.closeDatabaseConnection'();

// Connection to vo_probeReadings databaase
CustomKeywords.'connectionCmDatabase.CmConnection.connectDB'('localhost', 'vo_probeReadings', '5432', 'postgres', 'admin')

// Print new record created in vo_probeReadings.probehealth table
def query2 = "select readings from probehealth where probe_id = '" + probeId +"'"
def queryResult2 = CustomKeywords.'connectionCmDatabase.CmConnection.executeQuery'(query2)

while (queryResult2.next())
{
	def readings= queryResult2.getString(1)
		
	println('The readings is :' + readings)
	WS.verifyEqual(GlobalVariable.AgSenseReadings, readings)
}

CustomKeywords.'connectionCmDatabase.CmConnection.closeDatabaseConnection'();



