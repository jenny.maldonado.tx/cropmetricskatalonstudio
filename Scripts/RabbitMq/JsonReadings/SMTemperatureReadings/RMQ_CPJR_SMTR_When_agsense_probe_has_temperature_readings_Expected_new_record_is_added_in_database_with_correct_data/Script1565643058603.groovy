import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

// Take note that the reaginds value from the vo_probeReadings cannot be possible to be test by the number of the decimals retuned from the vo_agSenseProbeRawReadings database
// serial probe = 79810 , with only 1 valid reading in the current day

def probeId = '37'

//Connection DB
CustomKeywords.'connectionCmDatabase.CmConnection.connectDB'('localhost', 'vo_probeReadings', '5432', 'postgres', 'admin')

//Print new record created in vo_probeReadings.soilmoisturetemperature table
def query = "select probe_id from soilmoisturetemperature where probe_id =" + probeId
def queryResult = CustomKeywords.'connectionCmDatabase.CmConnection.executeQuery'(query)

while (queryResult.next())
{
	def probe_id = queryResult.getString(1)
	
	println('The readings is :' + probe_id )	
	
	WS.verifyEqual(probeId, probe_id)
}


//double, comparar con la respuesta del katalon del rawreadings database