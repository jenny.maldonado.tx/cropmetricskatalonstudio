import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable


// Connection to vo_ssurgoRawData databaase
CustomKeywords.'connectionCmDatabase.CmConnection.connectDB'('localhost', 'vo_ssurgoRawData', '5432', 'postgres', 'admin')

// Print new record created in vo_ssurgoRawData database
def query = 'select field_id from spatial.soildatawgs84geographic order by field_id asc'
def queryResult = CustomKeywords.'connectionCmDatabase.CmConnection.executeQuery'(query)

def fieldId = []
def count = 0
while (queryResult.next()) {
	field_id = queryResult.getString(1);
	fieldId[count] = field_id
	count= count +1
}

println('The number of the fields stored in the vo_ssurgoRawData database are: ' + fieldId)
CustomKeywords.'connectionCmDatabase.CmConnection.closeDatabaseConnection'()


// Connection to virtualoptimizer databaase
CustomKeywords.'connectionCmDatabase.CmConnection.connectDB'('localhost', 'virtualoptimizer', '5432', 'postgres', 'admin')

// Print new record created in virtualoptimizer database
def query2 = "select f.id from cropmetrics.field f inner join cropmetrics.farm fr  on fr.id = f.farm_id inner join cropmetrics.userprofile up on up.id = fr.userprofile_id where f.timezoneiana in ('America/Chicago', 'America/Denver') and f.isvalid = true and fr.isvalid = true and up.isvalid = true order by f.id asc"
def queryResult2 = CustomKeywords.'connectionCmDatabase.CmConnection.executeQuery'(query2)

def fieldId2 = []
def count2 = 0
while (queryResult2.next()) {
	field_id2 = queryResult2.getString(1);
	fieldId2[count2] = field_id2
	count2= count2 +1
}


if(fieldId2 != null)
{
	println('The field available in USA into virtualoptimizer database are: ' + fieldId2)
}

else
   {
	   assert(fieldId2.isEmpty())
   }

WS.verifyEqual(fieldId, fieldId2)

CustomKeywords.'connectionCmDatabase.CmConnection.closeDatabaseConnection'()