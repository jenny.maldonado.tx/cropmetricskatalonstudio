import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

import com.kms.katalon.core.testobject.ResponseObject as ResponseObject
import com.kms.katalon.core.testobject.RequestObject as RequestObject
import com.kms.katalon.core.testobject.impl.HttpTextBodyContent as HttpTextBodyContent

// Preconditions
// 1. Have at least one valid account as client
// 2. Have at least one farm/fields created in the account of the precondition 1
// 3. Have at least one agent created, and must have the user of the precondition 1 as grower assigned

// Get tokenfor the user of the precondition 1
def tokenUserName = 'jenny.maldonado.tx1@gmail.com'
def tokenPassword = 'cropmetrics'

// Post token
responseToken = WS.sendRequest(findTestObject('VoPro/Token', [('usernameToken') : tokenUserName, ('passwordToken') : tokenPassword]))

// Get body response attributes for the token
def slurper = new groovy.json.JsonSlurper()
def tokenResult = slurper.parseText(responseToken.getResponseBodyContent())

// Get result parameters of token
def accessToken = tokenResult.access_token

println('.. Current access token generated for admin user : ' + accessToken)

//Get Farm Field Users data
requestFarmGet = WS.sendRequest(findTestObject('VoGrow/Farm/FarmGet', [('access_token') : accessToken]))

// Get status code
//WS.verifyResponseStatusCode(requestFarmGet, 200)

// Get body response attributes for put field endpoint
def slurper1 = new groovy.json.JsonSlurper()
def responseBodyFarm = slurper1.parseText(requestFarmGet.getResponseBodyContent())
/////////////////////////////////
///////////////////////////////// Is not posible revire the response, because is not sorting and the order of the data is RAMDOM
/////////////////////////////////
//Connection DB
CustomKeywords.'connectionCmDatabase.CmConnection.connectDB'('localhost', 'virtualoptimizer', '5432', 'postgres', 'admin')

// Get database values
def query = "select up.id, f.id,f.name,fr.id, fr.name from cropmetrics.field f inner join cropmetrics.farm fr on fr.id = f.farm_id inner join cropmetrics.userprofile up on up.id = fr.userprofile_id where up.username = '" + tokenUserName + "'"
def queryResult = CustomKeywords.'connectionCmDatabase.CmConnection.executeQuery'(query)
def count = 0
while (queryResult.next())
{
	def userNameIdDB = queryResult.getString(1)
	def fieldIdDB = queryResult.getString(2)
	def nameFieldDB = queryResult.getString(3)
	def farmIdDB = queryResult.getString(4)
	def farmNameDB = queryResult.getString(5)

	
	
	//Print bosy response
	println('The farm id of the user token is:  ' + responseBodyFarm[count].id)
	println('The farm name of the user token is:  ' + responseBodyFarm[count].name)
	println('The user id of the user token is:  ' + responseBodyFarm[count].user_id)
	def responseId = '[' + count + '].id'
	
	println(responseId)
	
	// Verify body response with the dabatabase data
	WS.verifyElementPropertyValue(requestFarmGet, responseId, farmIdDB)
	WS.verifyElementPropertyValue(requestFarmGet, '['+count+'].name', farmNameDB)
	WS.verifyElementPropertyValue(requestFarmGet, '['+count+'].user_id', userNameIdDB)
	
	count =+ 1
}

////////////////////////////////
