import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

//Connection DB
CustomKeywords.'connectionCmDatabase.CmConnection.connectDB'('localhost', 'virtualoptimizer', '5432', 'postgres', 'admin')

println('New farm id is : ' + GlobalVariable.farmId)
println('Grower id taht is owner of the new farm, is : ' + GlobalVariable.userId)

//Print new farm values created in farm table
def query = 'select id, name, userprofile_id from cropmetrics.farm where id =' + GlobalVariable.farmId
def queryResult = CustomKeywords.'connectionCmDatabase.CmConnection.executeQuery'(query)

while (queryResult.next())
{
	def farmIdCreated = queryResult.getString(1)
	def farmNameCreated = queryResult.getString(2)
	def userProfileId = queryResult.getString(3)
	
	println('The farm id created is :' + farmIdCreated)
	println('The farm name created is :' + farmNameCreated)
	println('The owner (userprofile_id) of the new farm created is :' + userProfileId)
	
}

//Print new farm created in farmsubscription table
def query1 = 'select farm_id, currentfarmserviceleveltype_id, onexpirefarmserviceleveltype_id from cropmetrics.farmsubscription where currentfarmserviceleveltype_id = 0 and  onexpirefarmserviceleveltype_id = 0 and farm_id =' + GlobalVariable.farmId
def queryResult1 = CustomKeywords.'connectionCmDatabase.CmConnection.executeQuery'(query1)

while (queryResult1.next())
{
	def farmIdCreated = queryResult1.getString(1)
	def currentfarmserviceleveltype_id = queryResult1.getString(2)
	def onexpirefarmserviceleveltype_id = queryResult1.getString(3)
	
	println('The farm id created is :' + farmIdCreated)
	println('The new current farm service level type is :' + currentfarmserviceleveltype_id + ' , new farm is Free by default')
	println('The on expire farm service level type is :' + onexpirefarmserviceleveltype_id + ' , new farm is Free by default')
	
}

//Prin the number of the farms created pero user_profile of grower
def query = 'select count(userprofile_id), userprofile_id from cropmetrics.farm where userprofile_id =' + GlobalVariable.userId + 'and name =' + GlobalVariable.farmName + ' group by userprofile_id'
def queryResult = CustomKeywords.'connectionCmDatabase.CmConnection.executeQuery'(query)

while (queryResult.next())
{
	def nroFarmIdCreated = queryResult.getString(1)
	def userprofileId = queryResult.getString(2)
	
	
	println('The number of the farm ids created is/are :' + nroFarmIdCreated)
	println('The user profile id of the farms created is :' + userprofileId)
	
	if(nroFarmIdCreated > 1)
	{
		println('ERROR in the creation existe more than 1 farm created')
	}
	
}