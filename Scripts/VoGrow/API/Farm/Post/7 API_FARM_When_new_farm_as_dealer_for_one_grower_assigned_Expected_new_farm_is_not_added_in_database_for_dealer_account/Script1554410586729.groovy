import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

//Connection DB
CustomKeywords.'connectionCmDatabase.CmConnection.connectDB'('localhost', 'virtualoptimizer', '5432', 'postgres', 'admin')
def dealerId = 2

println('New farm id is : ' + GlobalVariable.farmId)
println('Dealer of the grower id that has the new farm, is : ' + dealerId)

//Print new farm values created in farm table
def query = 'select id, name, userprofile_id from cropmetrics.farm where id =' + GlobalVariable.farmId
def queryResult = CustomKeywords.'connectionCmDatabase.CmConnection.executeQuery'(query)

while (queryResult.next())
{
	def farmIdCreated = queryResult.getString(1)
	def farmNameCreated = queryResult.getString(2)
	def userProfileId = queryResult.getString(3)
	
	println('The farm id created is :' + farmIdCreated)
	println('The farm name created is :' + farmNameCreated)
	println('The owner (userprofile_id) of the new farm created is :' + userProfileId)
	
	if(userProfileId == dealerId)
	{
		println('ERROR the owner of the new farm created shoiul be the of the grower not of the dealer')
	}
}

