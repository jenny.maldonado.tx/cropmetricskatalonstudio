import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.testobject.ResponseObject as ResponseObject
import com.kms.katalon.core.testobject.RequestObject as RequestObject
import com.kms.katalon.core.testobject.impl.HttpTextBodyContent as HttpTextBodyContent

/*Preconditions
 1. Have valid account created
 2. Have at least 1 Farm, field created
 3. Have at least 1 probe with valid readings assigned in the same location of the field of the precondition 2
 */
//review the data created previously //
def tokenUserName = 'cropmetrics.agsense@gmail.com'
def tokenPassword = 'cropmetrics'
def famId = '7'
def fieldId = '32'
def startTimeStamp = '1546300800'
def endTimeStamp = '1577750400'

// metadata units values expected
def s1 = 'inches'
def s2 = 'inches'
def s3 = 'inches'
def s4 = 'inches'
def s5 = 'inches'
def s6 = 'inches'
def s7 = 'inches'
def s8 = 'inches'
def s9 = 'inches'
def s10 = 'inches'
def s11 = 'inches'
def s12 = 'inches'
def sm1 = 'inches'
def sm2 = 'inches'
def sm3 = 'inches'
def sm4 = 'inches'
def sm5 = 'inches'
def sm6 = 'inches'
def sm7 = 'inches'
def sm8 = 'inches'
def sm9 = 'inches'
def sm10 = 'inches'
def sm11 = 'inches'
def sm12 = 'inches'

// Post token
responseToken = WS.sendRequest(findTestObject('VoPro/Token', [('usernameToken') : tokenUserName, ('passwordToken') : tokenPassword]))

// Get body response attributes for the token
def slurper = new groovy.json.JsonSlurper()
def tokenResult = slurper.parseText(responseToken.getResponseBodyContent())

// Get result parameters of token
def accessToken = tokenResult.access_token

println('.. Current access token generated for admin user : ' + accessToken)

//Connection DB virtualoptimizer
CustomKeywords.'connectionCmDatabase.CmConnection.connectDB'('localhost', 'virtualoptimizer', '5432', 'postgres', 'admin')

// Get from database IanaTimeZone of the field
def query = 'select timezoneiana from cropmetrics.field where id = ' + fieldId
def queryResult = CustomKeywords.'connectionCmDatabase.CmConnection.executeQuery'(query)

while (queryResult.next()) {
    timeZoneIana = queryResult.getString(1)
}

//Close connection DB virtualoptimizer
CustomKeywords.'connectionCmDatabase.CmConnection.closeDatabaseConnection'()


//Connection DB vo_summarizedData
CustomKeywords.'connectionCmDatabase.CmConnection.connectDB'('localhost', 'vo_summarizedData', '5432', 'postgres', 'admin')

// Get from database Probe id of the field 
def queryPrbId = 'select  (elements->>\'id\')::int ProbeId from fieldsummarysoilmoisture cross join json_array_elements(probesummary::json) elements where field_id = ' + fieldId
def queryResultPrbId = CustomKeywords.'connectionCmDatabase.CmConnection.executeQuery'(queryPrbId)

while (queryResultPrbId.next()) {
    probeId = queryResultPrbId.getString(1)
}

//Close connection DB vo_summarizedData
CustomKeywords.'connectionCmDatabase.CmConnection.closeDatabaseConnection'()


//Connection DB virtualoptimizer
CustomKeywords.'connectionCmDatabase.CmConnection.connectDB'('localhost', 'virtualoptimizer', '5432', 'postgres', 'admin')

// Get from database probe sensor depth values of the probe id
def querySDepth = 'select s1, s2, s3, s4, s5, s6, s7, s8, s9, s10, s11, s12 from cropmetrics.probesensordepth where probe_id = ' + probeId
def queryResultSDepth = CustomKeywords.'connectionCmDatabase.CmConnection.executeQuery'(querySDepth)

while (queryResultSDepth.next()) {
	sDepth1 = queryResultSDepth.getString(1) + '.0'
	sDepth2 = queryResultSDepth.getString(2) + '.0'
	sDepth3 = queryResultSDepth.getString(3) + '.0'
	sDepth4 = queryResultSDepth.getString(4) + '.0'
	sDepth5 = queryResultSDepth.getString(5) + '.0'
	sDepth6 = queryResultSDepth.getString(6) + '.0'
	sDepth7 = queryResultSDepth.getString(7) + '.0'
	sDepth8 = queryResultSDepth.getString(8) + '.0'
	sDepth9 = queryResultSDepth.getString(9) + '.0'
	sDepth10 = queryResultSDepth.getString(10) + '.0'
	sDepth11 = queryResultSDepth.getString(11) + '.0'
	sDepth12 = queryResultSDepth.getString(12) + '.0'
}

//Close connection DB virtualoptimizer
CustomKeywords.'connectionCmDatabase.CmConnection.closeDatabaseConnection'()

//Connection DB vo_probeReadings
CustomKeywords.'connectionCmDatabase.CmConnection.connectDB'('localhost', 'vo_probeReadings', '5432', 'postgres', 'admin')

// Get from database probe sensor depth values of the probe id
// Note: s10, s11, s12, there are not readings in the Raw database
def queryPrbReadings = "select round((elements->>'s1')::numeric,8) as s1, round((elements->>'s2')::numeric,8) as s2,	round((elements->>'s3')::numeric,8) as s3, round((elements->>'s4')::numeric,8) as s4, round((elements->>'s5')::numeric,7) as s5, round((elements->>'s6')::numeric,8) as s6, round((elements->>'s7')::numeric,8) as s7, round((elements->>'s8')::numeric,8) as s8, round((elements->>'s9')::numeric,8) as s9, round((elements->>'s10')::numeric,8) as s10, round((elements->>'s11')::numeric,8) as s11, round((elements->>'s12')::numeric,8) as s12 from soilmoisture cross join json_array_elements(readings::json) elements where probe_id = " + probeId

def queryResultrbReadings = CustomKeywords.'connectionCmDatabase.CmConnection.executeQuery'(queryPrbReadings)

while (queryResultrbReadings.next()) {
	s1Reading = '[' + queryResultrbReadings.getString(1) + ']'
	s2Reading = '[' + queryResultrbReadings.getString(2) + ']'
	s3Reading = '[' + queryResultrbReadings.getString(3) + ']'
	s4Reading = '[' + queryResultrbReadings.getString(4) + ']'
	s5Reading = '[' + queryResultrbReadings.getString(5) + ']'
	s6Reading = '[' + queryResultrbReadings.getString(6) + ']'
	s7Reading = '[' + queryResultrbReadings.getString(7) + ']'
	s8Reading = '[' + queryResultrbReadings.getString(8) + ']'
	s9Reading = '[' + queryResultrbReadings.getString(9) + ']'
	s10Reading = '[0.0]'
	s11Reading = '[0.0]'
	s12Reading = '[0.0]'
}

//Close connection DB virtualoptimizer
CustomKeywords.'connectionCmDatabase.CmConnection.closeDatabaseConnection'()


//Get field soil moisture endpoint
requestFieldSMGet = WS.sendRequest(findTestObject('VoGrow/Field/FieldSoilMoisture', [('access_token') : accessToken, ('field_id') : fieldId, ('start_time_stamp') : startTimeStamp, ('end_time_stamp') : endTimeStamp]))

// Get status code
//WS.verifyResponseStatusCode(requestFieldSMGet, 200)

// Get body response attributes for put field endpoint
def slurper1 = new groovy.json.JsonSlurper()

def responseBodyFieldSoilMoisture = slurper1.parseText(requestFieldSMGet.getResponseBodyContent())

//Print body response for metadata section
println('The metadata  for s1 is:  ' + responseBodyFieldSoilMoisture.metadata.units.s1)
println('The metadata  for s2 is:  ' + responseBodyFieldSoilMoisture.metadata.units.s2)
println('The metadata  for s3 is:  ' + responseBodyFieldSoilMoisture.metadata.units.s3)
println('The metadata  for s4 is:  ' + responseBodyFieldSoilMoisture.metadata.units.s4)
println('The metadata  for s5 is:  ' + responseBodyFieldSoilMoisture.metadata.units.s5)
println('The metadata  for s6 is:  ' + responseBodyFieldSoilMoisture.metadata.units.s6)
println('The metadata  for s7 is:  ' + responseBodyFieldSoilMoisture.metadata.units.s7)
println('The metadata  for s8 is:  ' + responseBodyFieldSoilMoisture.metadata.units.s8)
println('The metadata  for s9 is:  ' + responseBodyFieldSoilMoisture.metadata.units.s9)
println('The metadata  for s10 is:  ' + responseBodyFieldSoilMoisture.metadata.units.s10)
println('The metadata  for s11 is:  ' + responseBodyFieldSoilMoisture.metadata.units.s11)
println('The metadata  for s12 is:  ' + responseBodyFieldSoilMoisture.metadata.units.s12)
println('The metadata  for sm1 is:  ' + responseBodyFieldSoilMoisture.metadata.units.sm1)
println('The metadata  for sm2 is:  ' + responseBodyFieldSoilMoisture.metadata.units.sm2)
println('The metadata  for sm3 is:  ' + responseBodyFieldSoilMoisture.metadata.units.sm3)
println('The metadata  for sm4 is:  ' + responseBodyFieldSoilMoisture.metadata.units.sm4)
println('The metadata  for sm5 is:  ' + responseBodyFieldSoilMoisture.metadata.units.sm5)
println('The metadata  for sm6 is:  ' + responseBodyFieldSoilMoisture.metadata.units.sm6)
println('The metadata  for sm7 is:  ' + responseBodyFieldSoilMoisture.metadata.units.sm7)
println('The metadata  for sm8 is:  ' + responseBodyFieldSoilMoisture.metadata.units.sm8)
println('The metadata  for sm9 is:  ' + responseBodyFieldSoilMoisture.metadata.units.sm9)
println('The metadata  for sm10 is:  ' + responseBodyFieldSoilMoisture.metadata.units.sm10)
println('The metadata  for sm11 is:  ' + responseBodyFieldSoilMoisture.metadata.units.sm11)
println('The metadata  for sm12 is:  ' + responseBodyFieldSoilMoisture.metadata.units.sm12)

println('The time_zone  for s1 is:  ' + responseBodyFieldSoilMoisture.time_zone)

println('The sensor_depths  for s1 is:  ' + responseBodyFieldSoilMoisture.sensor_depths.s1)
println('The sensor_depths  for s2 is:  ' + responseBodyFieldSoilMoisture.sensor_depths.s2)
println('The sensor_depths  for s3 is:  ' + responseBodyFieldSoilMoisture.sensor_depths.s3)
println('The sensor_depths  for s4 is:  ' + responseBodyFieldSoilMoisture.sensor_depths.s4)
println('The sensor_depths  for s5 is:  ' + responseBodyFieldSoilMoisture.sensor_depths.s5)
println('The sensor_depths  for s6 is:  ' + responseBodyFieldSoilMoisture.sensor_depths.s6)
println('The sensor_depths  for s7 is:  ' + responseBodyFieldSoilMoisture.sensor_depths.s7)
println('The sensor_depths  for s8 is:  ' + responseBodyFieldSoilMoisture.sensor_depths.s8)
println('The sensor_depths  for s9 is:  ' + responseBodyFieldSoilMoisture.sensor_depths.s9)
println('The sensor_depths  for s10 is:  ' + responseBodyFieldSoilMoisture.sensor_depths.s10)
println('The sensor_depths  for s11 is:  ' + responseBodyFieldSoilMoisture.sensor_depths.s11)
println('The sensor_depths  for s12 is:  ' + responseBodyFieldSoilMoisture.sensor_depths.s12)

println('The reading for s1 is: ' + responseBodyFieldSoilMoisture.readings.sm1)
println('The reading for s2 is: ' + responseBodyFieldSoilMoisture.readings.sm2)
println('The reading for s3 is: ' + responseBodyFieldSoilMoisture.readings.sm3)
println('The reading for s4 is: ' + responseBodyFieldSoilMoisture.readings.sm4)
println('The reading for s5 is: ' + responseBodyFieldSoilMoisture.readings.sm5)
println('The reading for s6 is: ' + responseBodyFieldSoilMoisture.readings.sm6)
println('The reading for s7 is: ' + responseBodyFieldSoilMoisture.readings.sm7)
println('The reading for s8 is: ' + responseBodyFieldSoilMoisture.readings.sm8)
println('The reading for s9 is: ' + responseBodyFieldSoilMoisture.readings.sm9)
println('The reading for s10 is: ' + responseBodyFieldSoilMoisture.readings.sm10)
println('The reading for s11 is: ' + responseBodyFieldSoilMoisture.readings.sm11)
println('The reading for s12 is: ' + responseBodyFieldSoilMoisture.readings.sm12)


// Verify body response with the dabatabase data
WS.verifyElementPropertyValue(requestFieldSMGet, 'metadata.units.s1', s1)
WS.verifyElementPropertyValue(requestFieldSMGet, 'metadata.units.s2', s2)
WS.verifyElementPropertyValue(requestFieldSMGet, 'metadata.units.s3', s3)
WS.verifyElementPropertyValue(requestFieldSMGet, 'metadata.units.s4', s4)
WS.verifyElementPropertyValue(requestFieldSMGet, 'metadata.units.s5', s5)
WS.verifyElementPropertyValue(requestFieldSMGet, 'metadata.units.s6', s6)
WS.verifyElementPropertyValue(requestFieldSMGet, 'metadata.units.s7', s7)
WS.verifyElementPropertyValue(requestFieldSMGet, 'metadata.units.s8', s8)
WS.verifyElementPropertyValue(requestFieldSMGet, 'metadata.units.s9', s9)
WS.verifyElementPropertyValue(requestFieldSMGet, 'metadata.units.s10', s10)
WS.verifyElementPropertyValue(requestFieldSMGet, 'metadata.units.s11', s11)
WS.verifyElementPropertyValue(requestFieldSMGet, 'metadata.units.s12', s12)
WS.verifyElementPropertyValue(requestFieldSMGet, 'metadata.units.sm1', sm1)
WS.verifyElementPropertyValue(requestFieldSMGet, 'metadata.units.sm2', sm2)
WS.verifyElementPropertyValue(requestFieldSMGet, 'metadata.units.sm3', sm3)
WS.verifyElementPropertyValue(requestFieldSMGet, 'metadata.units.sm4', sm4)
WS.verifyElementPropertyValue(requestFieldSMGet, 'metadata.units.sm5', sm5)
WS.verifyElementPropertyValue(requestFieldSMGet, 'metadata.units.sm6', sm6)
WS.verifyElementPropertyValue(requestFieldSMGet, 'metadata.units.sm7', sm7)
WS.verifyElementPropertyValue(requestFieldSMGet, 'metadata.units.sm8', sm8)
WS.verifyElementPropertyValue(requestFieldSMGet, 'metadata.units.sm9', sm9)
WS.verifyElementPropertyValue(requestFieldSMGet, 'metadata.units.sm10', sm10)
WS.verifyElementPropertyValue(requestFieldSMGet, 'metadata.units.sm11', sm11)
WS.verifyElementPropertyValue(requestFieldSMGet, 'metadata.units.sm12', sm12)

WS.verifyElementPropertyValue(requestFieldSMGet, 'time_zone', timeZoneIana)

WS.verifyElementPropertyValue(requestFieldSMGet, 'sensor_depths.s1', sDepth1)
WS.verifyElementPropertyValue(requestFieldSMGet, 'sensor_depths.s2', sDepth2)
WS.verifyElementPropertyValue(requestFieldSMGet, 'sensor_depths.s3', sDepth3)
WS.verifyElementPropertyValue(requestFieldSMGet, 'sensor_depths.s4', sDepth4)
WS.verifyElementPropertyValue(requestFieldSMGet, 'sensor_depths.s5', sDepth5)
WS.verifyElementPropertyValue(requestFieldSMGet, 'sensor_depths.s6', sDepth6)
WS.verifyElementPropertyValue(requestFieldSMGet, 'sensor_depths.s7', sDepth7)
WS.verifyElementPropertyValue(requestFieldSMGet, 'sensor_depths.s8', sDepth8)
WS.verifyElementPropertyValue(requestFieldSMGet, 'sensor_depths.s9', sDepth9)
WS.verifyElementPropertyValue(requestFieldSMGet, 'sensor_depths.s10', sDepth10)
WS.verifyElementPropertyValue(requestFieldSMGet, 'sensor_depths.s11', sDepth11)
WS.verifyElementPropertyValue(requestFieldSMGet, 'sensor_depths.s12', sDepth12)

WS.verifyElementPropertyValue(requestFieldSMGet, 'readings.sm1', s1Reading)
WS.verifyElementPropertyValue(requestFieldSMGet, 'readings.sm2', s2Reading)
WS.verifyElementPropertyValue(requestFieldSMGet, 'readings.sm3', s3Reading)
WS.verifyElementPropertyValue(requestFieldSMGet, 'readings.sm4', s4Reading)
WS.verifyElementPropertyValue(requestFieldSMGet, 'readings.sm5', s5Reading)
WS.verifyElementPropertyValue(requestFieldSMGet, 'readings.sm6', s6Reading)
WS.verifyElementPropertyValue(requestFieldSMGet, 'readings.sm7', s7Reading)
WS.verifyElementPropertyValue(requestFieldSMGet, 'readings.sm8', s8Reading)
WS.verifyElementPropertyValue(requestFieldSMGet, 'readings.sm9', s9Reading)
WS.verifyElementPropertyValue(requestFieldSMGet, 'readings.sm10', s10Reading)
WS.verifyElementPropertyValue(requestFieldSMGet, 'readings.sm11', s11Reading)
WS.verifyElementPropertyValue(requestFieldSMGet, 'readings.sm12', s12Reading)