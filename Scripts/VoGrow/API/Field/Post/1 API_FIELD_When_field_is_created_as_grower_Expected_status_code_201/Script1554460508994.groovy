import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.testobject.ResponseObject as ResponseObject
import com.kms.katalon.core.testobject.RequestObject as RequestObject
import com.kms.katalon.core.testobject.impl.HttpTextBodyContent as HttpTextBodyContent

// Preconditions
// 1. Have at least one valid account as client
// 2. Have at least one farm created in the account of the precondition 1

// Define attribute values for the new field,using the farm id of the precondition 2
def famId = '10'
def nameField = 'Field by katakon 2'
def boundaryField = '{"boundaries":[{"boundary":{"coords":[{"x":-101.49568751,"y":37.14976293},{"x":-101.50049403,"y":37.14647907},{"x":-101.50049403,"y":37.14264773},{"x":-101.49723246,"y":37.1411425},{"x":-101.49019435,"y":37.14223722},{"x":-101.48933604,"y":37.14620541},{"x":-101.49294093,"y":37.14866833},{"x":-101.49568751,"y":37.14976293}]}}]}'

// Define attribute values for probe
def dataServiceId = '1'
def externalProbeId = '54321'
def telemetryId = '54321'
def probeLatitude = '30.1886119910214'
def probeLongitude = '-93.47268104553223'
def probeType = '1'
def telemetryType = '1'
def installerName = 'test katalon2'
def soilMoisture = '[ {"depth": 1,	"soil_moisture": 0  },  {"depth": 2,"soil_moisture": 3 }, {"depth": 3,"soil_moisture": 3 }, {"depth": 4,"soil_moisture": 0 }]'
def soilType = '[ {"depth": 1,"soil_type": 0 }, {"depth": 2,"soil_type": 3 }, {"depth": 3,"soil_type": 3 }, {"depth": 4,"soil_type": 3 }]'
def note = '{"text":"Probe note test in the fld edition"}'

//Define attribute vlues fro crop
def cropTypeId = '1'
def cropVariety = 'Generic test katalon2'
def seedingRate = '123456'
def plantingDate = '2019-04-18'
def relativeMaturityId = '10'

// Get tokenfor the user of the precondition 1
def tokenUserName = 'cropmetricsvo.agentf@gmail.com'
def tokenPassword = 'cropmetricsvo'

responseToken = WS.sendRequest(findTestObject('VoPro/Token', [('usernameToken') : tokenUserName, ('passwordToken') : tokenPassword]))

// Get body response attributes for the token
def slurper = new groovy.json.JsonSlurper()
def tokenResult = slurper.parseText(responseToken.getResponseBodyContent())

// Get result parameters of token
def accessToken = tokenResult.access_token

println('.. Current access token generated for admin user : ' + accessToken)


// Post new farm for the user of the precondition 1
def requestFieldPost = ((findTestObject('VoGrow/Field/FieldCropPost', [('access_token') : accessToken])) as RequestObject)

String bodyRequest = '{"field":{"farm_id":' + famId + ',"name":"' + nameField + '","boundary":' + boundaryField + '},"crop": {"crop_type_id":' + cropTypeId + ',"crop_variety":"' + cropVariety + '","seeding_rate":' + seedingRate + ',"planting_date":"' + plantingDate + '","relative_maturity_id":' + relativeMaturityId + '},"probe":{"data_service_id":' + dataServiceId + ',"external_probe_id":"' + externalProbeId + '","telemetry_id":"' + telemetryId + '","probe_latitude":' + probeLatitude + ',"probe_longitude":' + probeLongitude + ',"probe_type":' + probeType + ',"telemetry_type":' + telemetryType + ',"installer_name":"' + installerName + '","soil_moisture":' + soilMoisture + ',"soil_type":' + soilType + ',"note":' + note + '}}'

try {
    requestFieldPost.setBodyContent(new HttpTextBodyContent(bodyRequest, 'UTF-8', 'application/json'))
}
catch (Exception ex) {
    println(ex.detailMessage)
} 

//Get body response
def responseField = WS.sendRequest(requestFieldPost)

// Get status code
WS.verifyResponseStatusCode(responseField, 201)

// Get body response attributes for post farm endpoint
def slurper1 = new groovy.json.JsonSlurper()
def responseBodyField = slurper1.parseText(responseField.getResponseBodyContent())

GlobalVariable.fieldId = responseBodyField.field.id

println('New field created has the next id:  ' + responseBodyField.field.id)
println('New field created has the next name:  ' + responseBodyField.field.name)
println('New field created has the farm id:  ' + responseBodyField.field.farm_id)

println('New field created has the new probe:  ' + responseBodyField.probe.message)
println('New field created has the new probe id:  ' + responseBodyField.probe.id)

println('New field created has the new crop:  ' + responseBodyField.crop.message)
println('New field created has the new crop id:  ' + responseBodyField.crop.id)

WS.verifyElementPropertyValue(responseField, 'field.farm_id', famId)

//test = WS.sendRequest(findTestObject('VoGrow/Field/FieldProbePost', [('access_token') : accessToken]))

