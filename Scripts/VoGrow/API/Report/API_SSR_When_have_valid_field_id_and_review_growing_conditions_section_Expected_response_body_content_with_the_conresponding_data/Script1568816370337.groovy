import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.testobject.ResponseObject as ResponseObject
import com.kms.katalon.core.testobject.RequestObject as RequestObject
import com.kms.katalon.core.testobject.impl.HttpTextBodyContent as HttpTextBodyContent
import groovy.json.JsonSlurper as JsonSlurper

// Preconditions
// 1. Have at least one valid account as client
// 2. Have at least one farm and field created in the account of the precondition 1
// 3. Have at least one probe assigned to the account of the precondition 1, take note that the probe must have onlly 1 setpoint defined in the current year

// Variables hardcoded for fieldId of the precondition 2 , to verify the body response of the endpoint
def fieldId = 1

//obtain file with the json expected

def contentFileExpected = new File("C:\\KatalonFiles\\SSR_Fld1.txt").getText('UTF-8').replaceAll(" ","").replaceAll("\r\n", "");

println('Content file.. : ' + contentFileExpected)

 
// Get token for the user of the precondition 1
def tokenUserName = 'cropmetrics.agentrw@gmail.com'
def tokenPassword = 'cropmetrics'

responseToken = WS.sendRequest(findTestObject('VoPro/Token', [('usernameToken') : tokenUserName, ('passwordToken') : tokenPassword]))

// Get body response attributes for the token
def slurper = new JsonSlurper()
def tokenResult = slurper.parseText(responseToken.getResponseBodyContent())

// Get result parameters of token
def accessToken = tokenResult.access_token

//Get field endpoint
requestGet = WS.sendRequest(findTestObject('VoGrow/Report/SeasonSummaryReport', [('access_token') : accessToken, ('field_id') : fieldId]))

def responseURI = requestGet.getResponseBodyContent().replaceAll(" ","").replaceAll("\r\n", "")

println('body response  endpoint... : ' + responseURI)


// Verify body response with the dabatabase data
WS.verifyEqual(contentFileExpected,responseURI)



