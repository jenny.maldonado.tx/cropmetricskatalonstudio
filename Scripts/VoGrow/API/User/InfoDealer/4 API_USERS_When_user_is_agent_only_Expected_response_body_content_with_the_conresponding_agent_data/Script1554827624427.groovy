import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

import com.kms.katalon.core.testobject.ResponseObject as ResponseObject
import com.kms.katalon.core.testobject.RequestObject as RequestObject
import com.kms.katalon.core.testobject.impl.HttpTextBodyContent as HttpTextBodyContent

// Preconditions
// 1. Have at least one valid account as grower

// Get tokenfor the user of the precondition 1
def tokenUserName = 'jenny.maldonado.tx1@gmail.com'
def tokenPassword = 'cropmetrics'

responseToken = WS.sendRequest(findTestObject('VoPro/Token', [('usernameToken') : tokenUserName, ('passwordToken') : tokenPassword]))

// Get body response attributes for the token
def slurper = new groovy.json.JsonSlurper()
def tokenResult = slurper.parseText(responseToken.getResponseBodyContent())

// Get result parameters of token
def accessToken = tokenResult.access_token

println('.. Current access token generated for admin user : ' + accessToken)


// Get user
responseUser = WS.sendRequest(findTestObject('VoGrow/User/Users', [('access_token') : accessToken]))

// Get status code
//WS.verifyResponseStatusCode(responseUser, 200)

// Get body response attributes for post farm endpoint
def slurper1 = new groovy.json.JsonSlurper()
def responseBodyUser = slurper1.parseText(responseUser.getResponseBodyContent())
 
//Print response values
println('The user id  is :  ' + responseBodyUser[0].id)
println('The user name is :  ' + responseBodyUser[0].name)
println('The user id  is :  ' + responseBodyUser[1].id)
println('The user name is :  ' + responseBodyUser[1].name)

//Database values for agent 'jenny.maldonado.tx1@gmail.com'
def idDBAgent = '2'
def nameDBAgent = 'Agent account'
//Database values for agent 'jenny.maldonado.tx1@gmail.com'
def idDBGrower = '3'
def nameDBGrower = 'grower 1  with agent'

//Verify response values for agent
WS.verifyElementPropertyValue(responseUser, '[0].id', idDBAgent)
WS.verifyElementPropertyValue(responseUser, '[0].name', nameDBAgent)

//Verify response values for grower
WS.verifyElementPropertyValue(responseUser, '[1].id', idDBGrower)
WS.verifyElementPropertyValue(responseUser, '[1].name', nameDBGrower)
