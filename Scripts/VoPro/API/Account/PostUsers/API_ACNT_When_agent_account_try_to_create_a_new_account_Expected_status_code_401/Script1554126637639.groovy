import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

// Define attribute values for agent account
def password = 'cropmetrics'
def email = 'cropmetricsvo.agent@gmail.com'
def fullname = 'test validation'
def phone = '123'
def dealer = 'x'
def addressLine1 = 'address line 1 test validation'
def addressLine2 = 'address line 2 test validation'
def town = 'townValidation'
def state = 'stateValidation'
def zip = 'xx'
def country = 'xx'
def roles = 'Agent'

// Get token
def tokenUserName = 'admin'
def tokenPassword = 'cropmetricsvo'

responseToken = WS.sendRequest(findTestObject('VoPro/Token', [('usernameToken') : tokenUserName, ('passwordToken') : tokenPassword]))

// Get responseToken body response attributes
def slurper = new groovy.json.JsonSlurper()
def tokenResult = slurper.parseText(responseToken.getResponseBodyContent())

// Get result parameters of token
def accessToken = tokenResult.access_token

println('.. Current access token generated for admin user : ' + accessToken)

// Post new account 
responseAdminRegister = WS.sendRequest(findTestObject('VoPro/Account/api_admin_v1_users', [('access_token') : accessToken, ('password') : password, ('email') : email
	, ('fullname') : fullname, ('phone') : phone, ('dealer') : dealer, ('addressLine1') : addressLine1, ('addressLine2') : addressLine2, ('town') : town
	, ('state') : state, ('zip') : zip, ('country') : country, ('roles') : roles]))


// Define attribute values the new account trying to create this by agent account
def passwordByAgent = 'cropmetrics'
def emailByAgent = 'cropmetricsvo.clientByAgent@gmail.com'
def fullnameByAgent = 'test validation'
def phoneByAgent = '123'
def dealerByAgent = 'x'
def addressLine1ByAgent = 'address line 1 test validation'
def addressLine2ByAgent = 'address line 2 test validation'
def townByAgent = 'townValidation'
def stateByAgent = 'stateValidation'
def zipByAgent = 'xx'
def countryByAgent = 'xx'
def rolesByAgent = 'Client'

// Get token
responseTokenByAgent = WS.sendRequest(findTestObject('VoPro/Token', [('usernameToken') : email, ('passwordToken') : password]))

// Get responseToken body response attributes
def slurperByAgent = new groovy.json.JsonSlurper()
def tokenResultByAgent = slurperByAgent.parseText(responseTokenByAgent.getResponseBodyContent())

// Get result parameters of token client account
def accessTokenByAgent = tokenResultByAgent.access_token

println('.. Current access token generated by the new client account : ' + accessTokenByAgent)

// Post new account
responseAdminRegisterByAgent = WS.sendRequest(findTestObject('VoPro/Account/api_admin_v1_users', [('access_token') : passwordByAgent, ('password') : passwordByAgent, ('email') : emailByAgent
	, ('fullname') : fullnameByAgent, ('phone') : phoneByAgent, ('dealer') : dealerByAgent, ('addressLine1') : addressLine1ByAgent, ('addressLine2') : addressLine2ByAgent, ('town') : townByAgent
	, ('state') : stateByAgent, ('zip') : zipByAgent, ('country') : countryByAgent, ('roles') : rolesByAgent]))

WS.verifyResponseStatusCode(responseAdminRegisterByAgent, 401)

// Get response message for AdminRegister endpoint
def slurper1 = new groovy.json.JsonSlurper()
def AdminRegisterResult = slurper1.parseText(responseAdminRegisterByAgent.getResponseBodyContent())
def messageResponse = AdminRegisterResult.message
def messageExpected = "Authorization has been denied for this request."

//Print message response
println('Error message response: ' + messageResponse)
println('Error message expected: ' + messageExpected)

WS.verifyElementPropertyValue(responseAdminRegisterByAgent, 'message', messageExpected)
