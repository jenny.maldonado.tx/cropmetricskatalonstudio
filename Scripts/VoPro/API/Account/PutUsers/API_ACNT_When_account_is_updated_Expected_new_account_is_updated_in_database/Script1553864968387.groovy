import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

//Connection DB
CustomKeywords.'connectionCmDatabase.CmConnection.connectDB'('localhost', 'virtualoptimizer', '5432', 'postgres', 'admin')

println('Account updated id is : ' + GlobalVariable.userId)

//Print the userprofile values updated in userprofile table
def allquery = 'select username, email, name, phone, dealername, addressline1,addressline2, town, state, zip, country from cropmetrics.userprofile where id = ' + GlobalVariable.userId
def queryResultall = CustomKeywords.'connectionCmDatabase.CmConnection.executeQuery'(allquery)


while (queryResultall.next())
{
	def usernameUpdated = queryResultall.getString(1)
	def emailUpdated = queryResultall.getString(2)
	def nameUpdated = queryResultall.getString(3)
	def phoneUpdated = queryResultall.getString(4)
	def dealernameUpdated = queryResultall.getString(5)
	def addressline1Updated = queryResultall.getString(6)
	def addressline2Updated = queryResultall.getString(7)
	def townUpdated = queryResultall.getString(8)
	def stateUpdated = queryResultall.getString(9)
	def zipUpdated = queryResultall.getString(10)
	def countryUpdated = queryResultall.getString(11)
	

	println('The username updated is :' + usernameUpdated + ',verify the spaces')
	println('   The email updated is :' + emailUpdated + ',verify the spaces')
	println('The name updated is :' + nameUpdated)
	println('The phone updated is :' + phoneUpdated)
	println('The dealer updated is :' + dealernameUpdated)
	println('The address line 1 updated is :' + addressline1Updated)
	println('The address line 2 updated is :' + addressline2Updated)
	println('The town updated is :' + townUpdated)
	println('The state updated is :' + stateUpdated)
	println('The zip updated is :' + zipUpdated)
	println('The country updated is :' + countryUpdated)
		
}


//Print the usrId usernaem, and userprofile_id of the userprofile updated in user table
def query1 = 'select id,username,userprofile_id  from cropmetrics.user where userprofile_id = ' + GlobalVariable.userId
def queryResult1 = CustomKeywords.'connectionCmDatabase.CmConnection.executeQuery'(query1)

while (queryResult1.next())
{
	def userIdUPdated = queryResult1.getString(1)
	def userNameUPdated = queryResult1.getString(2)
	def userProfileIdUPdated = queryResult1.getString(3)

	println('The account updated has the next user_id in user table :' + userIdUPdated)
	println('The account updated has the next username in user table :' + userNameUPdated + ',verify the spaces')
	println('The account updated has the next userprofile_id in user table :' + userProfileIdUPdated)
	
}

//Print the userroleId in userrole table of the account updated
def query2 = 'select ur.id  from cropmetrics.userprofile upp inner join cropmetrics.user u  on u.userprofile_id=upp.id inner join cropmetrics.userrole ur  on ur.user_id = u.id where upp.id =  ' + GlobalVariable.userId
def queryResult2 = CustomKeywords.'connectionCmDatabase.CmConnection.executeQuery'(query2)

while (queryResult2.next())
{
	def userRoleUpdated = queryResult2.getString(1)

	println('The account updated has the next userRoleId :' + userRoleUpdated)
	
}


//Print the role name assigned of the account updated
def query3 = 'select r.name from cropmetrics.userprofile upp inner join cropmetrics.user u  on u.userprofile_id=upp.id inner join cropmetrics.userrole ur on ur.user_id = u.id inner join cropmetrics.role r on r.id = ur.role_id where upp.id = ' + GlobalVariable.userId
def queryResult3 = CustomKeywords.'connectionCmDatabase.CmConnection.executeQuery'(query3)

while (queryResult3.next())
{
	def userRoleName = queryResult3.getString(1)

	println('The account updated has the next user role name assigned :' + userRoleName)
}

//Print the season assigned in userprofileseason table of the account updated
def query4 = 'select s.name from cropmetrics.userprofileseason ups inner join cropmetrics.season s on s.id = ups.season_id where ups.userprofile_id =  ' + GlobalVariable.userId
def queryResult4 = CustomKeywords.'connectionCmDatabase.CmConnection.executeQuery'(query4)

while (queryResult4.next())
{
	def userSeasonAdded = queryResult4.getString(1)

	println('The account updated has the next season name assigned :' + userSeasonAdded)
}

