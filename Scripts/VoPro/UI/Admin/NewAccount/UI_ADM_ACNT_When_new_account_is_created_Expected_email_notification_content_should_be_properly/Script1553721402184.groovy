import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

/*
 grower email subject: CropMetrics VO Grow Sign-In
 
 grower email body text:
 "Below you will find your sign-in information for VO Grow. Please keep it in a safe place.
 Account Email: ________
 Password: _________
 
 To download and install the VO Grow app:
 Apple Store download [hyperlink]
 Google Play download [hyperlink]
 
 Once logged in, you will have the option to create a different password under User Profile.
 
 Returning VO customers: note that your login will be your email address and not your previous username. Contact CropMetrics support[email hyperlink to support@cropmetrics.com] for any needed assistance.
 Thank you,
 The CropMetrics Team
 */