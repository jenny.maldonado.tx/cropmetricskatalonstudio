<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Manual_Iteris_Weather_DailyForecast</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>dd4540b5-eccd-4d85-8be4-a88ce4fad429</testSuiteGuid>
   <testCaseLink>
      <guid>cd27f51b-6bfa-41f8-82d7-befa79c6ffab</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/DataIngest/Iteris/Weather/Forecast/DailyForecast/DI_W_DF_When_custom_hourtorun_and_minutestorun_values_Expected_process_start_according_values_configured_in_server_time</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>860ce617-4f9c-4cb1-a770-f5cb847c4942</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/DataIngest/Iteris/Weather/DB_Fields_WeatherDailyHistorical_WeatherDailyForecast</testDataId>
      </testDataLink>
   </testCaseLink>
   <testCaseLink>
      <guid>d44a48b1-5b58-4e7a-bf35-07e15620296b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/DataIngest/Iteris/Weather/Forecast/DailyForecast/DI_W_DF_When_field_does_not_exist_Expected_error_message_should_generate_in_log_files</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>79c87f55-7240-4566-ab3b-10dab1905273</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/DataIngest/Iteris/Weather/DB_Fields_WeatherDailyHistorical_WeatherDailyForecast</testDataId>
      </testDataLink>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>999429ab-07b8-4690-a43b-f4dcbfaf4e8a</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/DataIngest/Iteris/Weather/Forecast/DailyForecast/Xls_Iteris_WeatherDailyForecast_RabbitMq_Messages</testDataId>
      </testDataLink>
   </testCaseLink>
   <testCaseLink>
      <guid>fd3767ff-1ce0-488e-a5ac-19c3cd38d832</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/DataIngest/Iteris/Weather/Forecast/DailyForecast/DI_W_DF_When_field_flag_isvalid_is_false_Expected_error_message_should_generate_in_log_files</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>46149023-8409-453a-b417-fd4a9e002be6</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/DataIngest/Iteris/Weather/DB_Fields_WeatherDailyHistorical_WeatherDailyForecast</testDataId>
      </testDataLink>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>1726906e-1a19-48b4-a820-89b75e1a98b4</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/DataIngest/Iteris/Weather/Forecast/DailyForecast/Xls_Iteris_WeatherDailyForecast_RabbitMq_Messages</testDataId>
      </testDataLink>
   </testCaseLink>
   <testCaseLink>
      <guid>c334d4f2-9847-493e-a07f-7e490fe081a7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/DataIngest/Iteris/Weather/Forecast/DailyForecast/DI_W_DF_When_field_flag_isvalid_is_false_Expected_field_does_not_genereate_any_message</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>ec8c5acd-9f75-49bf-92a8-09bef4e46976</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/DataIngest/Iteris/Weather/DB_Fields_WeatherDailyHistorical_WeatherDailyForecast</testDataId>
      </testDataLink>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>c8ea83a0-1fb7-45b7-9961-76a426103844</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/DataIngest/Iteris/Weather/Forecast/DailyForecast/Xls_Iteris_WeatherDailyForecast_RabbitMq_Messages</testDataId>
      </testDataLink>
   </testCaseLink>
   <testCaseLink>
      <guid>fc2b15d6-66e3-4dd2-b17e-6f6ca2449a91</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/DataIngest/Iteris/Weather/Forecast/DailyForecast/DI_W_DF_When_field_is_char_Expected_error_message_should_generate_in_log_files</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>bceb4ecf-c579-4744-b538-7d384aaef174</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/DataIngest/Iteris/Weather/DB_Fields_WeatherDailyHistorical_WeatherDailyForecast</testDataId>
      </testDataLink>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>d773a31d-7de4-4701-90b8-5c09df4fa5d9</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/DataIngest/Iteris/Weather/Forecast/DailyForecast/Xls_Iteris_WeatherDailyForecast_RabbitMq_Messages</testDataId>
      </testDataLink>
   </testCaseLink>
   <testCaseLink>
      <guid>63352fe9-606b-4295-b69c-3eb0031437dc</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/DataIngest/Iteris/Weather/Forecast/DailyForecast/DI_W_DF_When_field_parameter_is_empty_Expected_error_message_should_generate_in_log_files</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>322a778e-04d5-4271-9aa7-f6d75d4ae6ac</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/DataIngest/Iteris/Weather/DB_Fields_WeatherDailyHistorical_WeatherDailyForecast</testDataId>
      </testDataLink>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>00319a68-9005-4024-bd82-bad9741c9d5b</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/DataIngest/Iteris/Weather/Forecast/DailyForecast/Xls_Iteris_WeatherDailyForecast_RabbitMq_Messages</testDataId>
      </testDataLink>
   </testCaseLink>
   <testCaseLink>
      <guid>6488b67e-88ef-46c4-a077-8d9c5a1de6e9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/DataIngest/Iteris/Weather/Forecast/DailyForecast/DI_W_DF_When_have_1_valid_field_id_Expected_new_row_should_stored_in_dailyForecast_table_of_iterisrawdata</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>23e0ac15-bc4d-4ad0-863b-1756eb71407a</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/DataIngest/Iteris/Weather/DB_Fields_WeatherDailyHistorical_WeatherDailyForecast</testDataId>
      </testDataLink>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>6c403c49-e5b7-4134-a5f5-f1bc6afe6baa</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/DataIngest/Iteris/Weather/Forecast/DailyForecast/Xls_Iteris_WeatherDailyForecast_RabbitMq_Messages</testDataId>
      </testDataLink>
   </testCaseLink>
   <testCaseLink>
      <guid>d50cfb47-b7bd-4338-a547-4883ac71fad8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/DataIngest/Iteris/Weather/Forecast/DailyForecast/DI_W_DF_When_have_5_field_identifiers_are_grouped_with_the_same_time_zone_Expected_message_per_field_id_is_generated</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>9c2cb28f-a551-47ab-9f64-d19c148a6d2e</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/DataIngest/Iteris/Weather/DB_Fields_WeatherDailyHistorical_WeatherDailyForecast</testDataId>
      </testDataLink>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>3ae6457e-014c-4a92-ac7f-50215cf91d7e</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/DataIngest/Iteris/Weather/Forecast/DailyForecast/Xls_Iteris_WeatherDailyForecast_RabbitMq_Messages</testDataId>
      </testDataLink>
   </testCaseLink>
   <testCaseLink>
      <guid>92c9b356-649d-4bd5-b34a-9bc5607e44db</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/DataIngest/Iteris/Weather/Forecast/DailyForecast/DI_W_DF_When_have_5_valid_fields_in_different_time_zone_Expected_error_message_should_generate_in_log_files</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>ef9b310d-0734-4e82-b612-db73a918a930</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/DataIngest/Iteris/Weather/DB_Fields_WeatherDailyHistorical_WeatherDailyForecast</testDataId>
      </testDataLink>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>8750c200-2dd1-4562-99b6-e67485f3a401</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/DataIngest/Iteris/Weather/Forecast/DailyForecast/Xls_Iteris_WeatherDailyForecast_RabbitMq_Messages</testDataId>
      </testDataLink>
   </testCaseLink>
   <testCaseLink>
      <guid>e9232c2e-d2a6-4161-a509-4dd7dea60a65</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/DataIngest/Iteris/Weather/Forecast/DailyForecast/DI_W_DF_When_have_5_valid_fields_in_the_same_time_zone_Expected_new_row_should_be_stored_in_the_database_per_field</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>d7a1033c-a87a-4fa1-885f-b8e0b2302928</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/DataIngest/Iteris/Weather/DB_Fields_WeatherDailyHistorical_WeatherDailyForecast</testDataId>
      </testDataLink>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>2f8e684e-c8a7-4462-b40a-a10bbf00f142</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/DataIngest/Iteris/Weather/Forecast/DailyForecast/Xls_Iteris_WeatherDailyForecast_RabbitMq_Messages</testDataId>
      </testDataLink>
   </testCaseLink>
   <testCaseLink>
      <guid>929fc4ad-fec3-4c84-ab2b-376dc68b011e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/DataIngest/Iteris/Weather/Forecast/DailyForecast/DI_W_DF_When_have_field_identifiers_in_different_time_zone_Expected_message_per_field_id_is_generated</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>9e54a2ef-5cef-4e37-92db-0d3028acd5f7</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/DataIngest/Iteris/Weather/DB_Fields_WeatherDailyHistorical_WeatherDailyForecast</testDataId>
      </testDataLink>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>ef9acb0e-1a96-4142-8dee-71e899a8b8f3</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/DataIngest/Iteris/Weather/Forecast/DailyForecast/Xls_Iteris_WeatherDailyForecast_RabbitMq_Messages</testDataId>
      </testDataLink>
   </testCaseLink>
   <testCaseLink>
      <guid>98338370-00b4-4c7b-b766-f97eebae45f9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/DataIngest/Iteris/Weather/Forecast/DailyForecast/DI_W_DF_When_have_more_than_5_valid_fields_Expected_error_message_should_generate_in_log_files</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>f663506c-f229-4554-a6fc-bc2c47860349</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/DataIngest/Iteris/Weather/DB_Fields_WeatherDailyHistorical_WeatherDailyForecast</testDataId>
      </testDataLink>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>8a5416c2-5d4a-4e7e-9088-1ae3f234ac9c</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/DataIngest/Iteris/Weather/Forecast/DailyForecast/Xls_Iteris_WeatherDailyForecast_RabbitMq_Messages</testDataId>
      </testDataLink>
   </testCaseLink>
   <testCaseLink>
      <guid>819569c1-cbd6-469b-b992-fcea29fdfdf7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/DataIngest/Iteris/Weather/Forecast/DailyForecast/DI_W_DF_When_have_valid_field_Expected_all_data_should_overwriting_from_current_day_to_next_7_days</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>ec6d759d-aec2-4c24-a8bc-e2ea8bc23b9b</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/DataIngest/Iteris/Weather/DB_Fields_WeatherDailyHistorical_WeatherDailyForecast</testDataId>
      </testDataLink>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>a0ad6bfc-713e-4b72-a716-b37679ce8dd9</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/DataIngest/Iteris/Weather/Forecast/DailyForecast/Xls_Iteris_WeatherDailyForecast_RabbitMq_Messages</testDataId>
      </testDataLink>
   </testCaseLink>
   <testCaseLink>
      <guid>67882365-f618-41ba-899d-110692552c9e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/DataIngest/Iteris/Weather/Forecast/DailyForecast/DI_W_DF_When_have_valid_field_Expected_data_stored_should_the_same_of_Iteris_endpoint_daily_forecast_v1.1</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>ac43eb20-d289-435b-bb19-7707e05544b5</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/DataIngest/Iteris/Weather/DB_Fields_WeatherDailyHistorical_WeatherDailyForecast</testDataId>
      </testDataLink>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>763ca150-6334-4c63-9da9-16489dd3904b</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/DataIngest/Iteris/Weather/Forecast/DailyForecast/Xls_Iteris_WeatherDailyForecast_RabbitMq_Messages</testDataId>
      </testDataLink>
   </testCaseLink>
   <testCaseLink>
      <guid>a6089624-46d5-4e9f-9561-6b97280c3756</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/DataIngest/Iteris/Weather/Forecast/DailyForecast/DI_W_DF_When_have_valid_field_Expected_data_stored_should_consider_the_next_7_days</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>a76b78a2-1b7e-4bc1-adfa-6d498d67c27f</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/DataIngest/Iteris/Weather/DB_Fields_WeatherDailyHistorical_WeatherDailyForecast</testDataId>
      </testDataLink>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>9fe45603-d289-4743-8fed-5b3b8e4d17db</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/DataIngest/Iteris/Weather/Forecast/DailyForecast/Xls_Iteris_WeatherDailyForecast_RabbitMq_Messages</testDataId>
      </testDataLink>
   </testCaseLink>
   <testCaseLink>
      <guid>07c0dc28-32b5-4b75-91b6-34dbf1cc0cff</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/DataIngest/Iteris/Weather/Forecast/DailyForecast/DI_W_DF_When_have_valid_field_Expected_data_stored_should_start_in_current_day</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>f43d0cd7-dd52-4578-bd3c-ec2e66a82f26</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/DataIngest/Iteris/Weather/DB_Fields_WeatherDailyHistorical_WeatherDailyForecast</testDataId>
      </testDataLink>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>5d5085a7-875e-4092-b415-791312029561</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/DataIngest/Iteris/Weather/Forecast/DailyForecast/Xls_Iteris_WeatherDailyForecast_RabbitMq_Messages</testDataId>
      </testDataLink>
   </testCaseLink>
   <testCaseLink>
      <guid>9527f064-20be-4214-bec2-bb5cb8bc62a9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/DataIngest/Iteris/Weather/Forecast/DailyForecast/DI_W_DF_When_start_every_publishafterminutes_configure_and_have_new_fields_Expected_message_is_generated_only_for_new_field</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>3c3abf72-a4f0-4490-825c-482875e66c89</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/DataIngest/Iteris/Weather/DB_Fields_WeatherDailyHistorical_WeatherDailyForecast</testDataId>
      </testDataLink>
   </testCaseLink>
   <testCaseLink>
      <guid>af7130ad-a382-4b05-8277-88c2415318e8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/DataIngest/Iteris/Weather/Forecast/DailyForecast/DI_W_DF_When_time_zone_is_less_than_3_am_Expected_field_is_not_consider_to_generat_a_messege</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>a0a58f39-bebc-4040-bd7c-95bdeb022b64</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/DataIngest/Iteris/Weather/DB_Fields_WeatherDailyHistorical_WeatherDailyForecast</testDataId>
      </testDataLink>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>e19fcedf-4737-4ed5-82ea-f3f43b55910b</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/DataIngest/Iteris/Weather/Forecast/DailyForecast/Xls_Iteris_WeatherDailyForecast_RabbitMq_Messages</testDataId>
      </testDataLink>
   </testCaseLink>
   <testCaseLink>
      <guid>7677d1dd-ec67-4fde-9090-84ddd22e2b92</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/DataIngest/Iteris/Weather/Forecast/DailyForecast/DI_W_DF_When_time_zone_is_more_than_3_am_Expected_field_is_consider_to_generat_a_messege</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>eaaffc93-33dc-4886-b4b6-02690895083f</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/DataIngest/Iteris/Weather/DB_Fields_WeatherDailyHistorical_WeatherDailyForecast</testDataId>
      </testDataLink>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>e31d6c36-02a8-4e88-949d-1749f4db0ebe</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/DataIngest/Iteris/Weather/Forecast/DailyForecast/Xls_Iteris_WeatherDailyForecast_RabbitMq_Messages</testDataId>
      </testDataLink>
   </testCaseLink>
</TestSuiteEntity>
