<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Manual_Iteris_Weather_Forecast_Summary</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>695bacc2-24e1-40a1-a0ae-636fc94bc3c3</testSuiteGuid>
   <testCaseLink>
      <guid>5c894a95-225b-435e-9fa5-89d0bf19f4ff</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/DataIngest/Iteris/Weather/Forecast/Summary/DI_W_SF_When_1_field_id_as_char_Expected_no_new_data_should_store_in_fieldsummaryweatherforecast_table</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>11afafac-ab27-48fc-903d-50973625323c</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/DataIngest/Iteris/Weather/DB_Fields_WeatherDailyHistorical_WeatherDailyForecast</testDataId>
      </testDataLink>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>dcecb529-3c54-4e7d-9dd6-f64760179afe</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/DataIngest/Iteris/Weather/Forecast/Summary/Xls_Iteris_Weather_Forecast_Summary_RabbitMq_Messages</testDataId>
      </testDataLink>
   </testCaseLink>
   <testCaseLink>
      <guid>1fec3e7f-42c5-4c48-8d71-467db3e38b3c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/DataIngest/Iteris/Weather/Forecast/Summary/DI_W_SF_When_1_field_is_not_in_iterisrawdata_Expected_no_new_data_should_store_in_fieldsummaryweatherforecast</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>9329a885-9389-46b7-8bbf-f265e570a7c9</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/DataIngest/Iteris/Weather/DB_Fields_WeatherDailyHistorical_WeatherDailyForecast</testDataId>
      </testDataLink>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>ce977f3a-0c16-4317-aa68-7327e294a81d</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/DataIngest/Iteris/Weather/Forecast/Summary/Xls_Iteris_Weather_Forecast_Summary_RabbitMq_Messages</testDataId>
      </testDataLink>
   </testCaseLink>
   <testCaseLink>
      <guid>e6de204c-db44-4773-8e63-93b39277c719</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/DataIngest/Iteris/Weather/Forecast/Summary/DI_W_SF_When_1_field_with_flag_isvalid_false_Expected_no_data_should_store_in_fieldsummaryweatherforecast</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>1186429b-fb0a-42b1-bc10-823f92472e33</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/DataIngest/Iteris/Weather/DB_Fields_WeatherDailyHistorical_WeatherDailyForecast</testDataId>
      </testDataLink>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>4a05f7d1-2177-4eb4-b6f9-85157364cf2e</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/DataIngest/Iteris/Weather/Forecast/Summary/Xls_Iteris_Weather_Forecast_Summary_RabbitMq_Messages</testDataId>
      </testDataLink>
   </testCaseLink>
   <testCaseLink>
      <guid>18f3e56d-0508-4d2e-9e2c-072daedd57c6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/DataIngest/Iteris/Weather/Forecast/Summary/DI_W_SF_When_1_field_with_invalid_id_Expected_no_new_data_should_store_in_fieldsummaryweatherforecast_table</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>1a768501-b5a6-460a-9431-f7efeea29a45</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/DataIngest/Iteris/Weather/Forecast/Summary/Xls_Iteris_Weather_Forecast_Summary_RabbitMq_Messages</testDataId>
      </testDataLink>
   </testCaseLink>
   <testCaseLink>
      <guid>72868745-2afe-495b-8266-9adb2fd188fa</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/DataIngest/Iteris/Weather/Forecast/Summary/DI_W_SF_When_1_valid_field_Expected_field_id_and_weathersummary_values_should_store_of_precip_prob_for_forecastday1_</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>e54637db-e9e1-4172-a539-95eb71d78d5c</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/DataIngest/Iteris/Weather/DB_Fields_WeatherDailyHistorical_WeatherDailyForecast</testDataId>
      </testDataLink>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>fb517594-c19d-4348-aeaf-187f29486aa3</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/DataIngest/Iteris/Weather/Forecast/Summary/DB_IterisRawData_Weather_Forecast_Summary</testDataId>
      </testDataLink>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>2b4cbdc1-02f9-4a05-b38b-5fed35bc86e8</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/DataIngest/Iteris/Weather/Forecast/Summary/Xls_Iteris_Weather_Forecast_Summary_RabbitMq_Messages</testDataId>
      </testDataLink>
   </testCaseLink>
   <testCaseLink>
      <guid>60e1e327-1ef5-4921-968b-0a61a87bee0e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/DataIngest/Iteris/Weather/Forecast/Summary/DI_W_SF_When_1_valid_field_Expected_field_id_and_weathersummary_values_should_store_of_precip_prob_for_forecastday2_</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>52aa3098-f9c9-475e-96a6-0d2942c38232</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/DataIngest/Iteris/Weather/DB_Fields_WeatherDailyHistorical_WeatherDailyForecast</testDataId>
      </testDataLink>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>c8137ab7-06f7-4d75-8325-32d32ef26afd</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/DataIngest/Iteris/Weather/Forecast/Summary/DB_IterisRawData_Weather_Forecast_Summary</testDataId>
      </testDataLink>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>623fb857-da0a-4d31-900e-859150a65b79</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/DataIngest/Iteris/Weather/Forecast/Summary/Xls_Iteris_Weather_Forecast_Summary_RabbitMq_Messages</testDataId>
      </testDataLink>
   </testCaseLink>
   <testCaseLink>
      <guid>15aa7a6f-5d27-4ed2-aa00-b23c2a23a71d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/DataIngest/Iteris/Weather/Forecast/Summary/DI_W_SF_When_1_valid_field_Expected_field_id_and_weathersummary_values_should_store_of_precip_prob_for_forecastday3_</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>d6dccc3c-1d38-4dec-875f-2719cdfdf3ba</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/DataIngest/Iteris/Weather/DB_Fields_WeatherDailyHistorical_WeatherDailyForecast</testDataId>
      </testDataLink>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>34d5a904-9a18-4690-b259-574510c7a2fa</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/DataIngest/Iteris/Weather/Forecast/Summary/DB_IterisRawData_Weather_Forecast_Summary</testDataId>
      </testDataLink>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>edd17875-dba7-430a-a212-a2e28b839144</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/DataIngest/Iteris/Weather/Forecast/Summary/Xls_Iteris_Weather_Forecast_Summary_RabbitMq_Messages</testDataId>
      </testDataLink>
   </testCaseLink>
   <testCaseLink>
      <guid>b74c0699-2222-4def-b0f4-9212354b15ca</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/DataIngest/Iteris/Weather/Forecast/Summary/DI_W_SF_When_1_valid_field_Expected_field_id_and_weathersummary_values_should_store_of_precip_prob_for_forecastday4_</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>8bc3a878-62f4-4319-ad7a-015389de53da</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/DataIngest/Iteris/Weather/DB_Fields_WeatherDailyHistorical_WeatherDailyForecast</testDataId>
      </testDataLink>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>c7cd9c6c-e1c9-4610-8d50-dbf671261790</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/DataIngest/Iteris/Weather/Forecast/Summary/DB_IterisRawData_Weather_Forecast_Summary</testDataId>
      </testDataLink>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>2ffcc86c-48f2-4546-8a80-f88d47ab1731</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/DataIngest/Iteris/Weather/Forecast/Summary/Xls_Iteris_Weather_Forecast_Summary_RabbitMq_Messages</testDataId>
      </testDataLink>
   </testCaseLink>
   <testCaseLink>
      <guid>19fbfac1-f47b-4d50-9c13-e325e9ec3439</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/DataIngest/Iteris/Weather/Forecast/Summary/DI_W_SF_When_1_valid_field_Expected_field_id_and_weathersummary_values_should_store_of_precip_prob_for_forecastday5_</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>13cf46b5-5c75-419d-abec-f5e113550c25</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/DataIngest/Iteris/Weather/DB_Fields_WeatherDailyHistorical_WeatherDailyForecast</testDataId>
      </testDataLink>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>d412a0cb-39f9-4a07-9d80-3f2e2a4b0f4a</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/DataIngest/Iteris/Weather/Forecast/Summary/DB_IterisRawData_Weather_Forecast_Summary</testDataId>
      </testDataLink>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>76df94ac-18b7-4150-af61-dbed70be2d16</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/DataIngest/Iteris/Weather/Forecast/Summary/Xls_Iteris_Weather_Forecast_Summary_RabbitMq_Messages</testDataId>
      </testDataLink>
   </testCaseLink>
   <testCaseLink>
      <guid>91d7ede5-28ae-43ac-8c9c-48d789502845</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/DataIngest/Iteris/Weather/Forecast/Summary/DI_W_SF_When_1_valid_field_Expected_field_id_and_weathersummary_values_should_store_of_precip_prob_for_forecastday6_</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>3fe20aef-8d05-46c9-a1d7-49366615d449</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/DataIngest/Iteris/Weather/DB_Fields_WeatherDailyHistorical_WeatherDailyForecast</testDataId>
      </testDataLink>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>f80bcc4b-c8a1-49c7-878f-4ee99c983900</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/DataIngest/Iteris/Weather/Forecast/Summary/DB_IterisRawData_Weather_Forecast_Summary</testDataId>
      </testDataLink>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>50983e26-1ab1-4c0a-95ee-fbbcbd64ca68</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/DataIngest/Iteris/Weather/Forecast/Summary/Xls_Iteris_Weather_Forecast_Summary_RabbitMq_Messages</testDataId>
      </testDataLink>
   </testCaseLink>
   <testCaseLink>
      <guid>51d0d7a9-ea93-42c6-a7f5-621d2842df63</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/DataIngest/Iteris/Weather/Forecast/Summary/DI_W_SF_When_1_valid_field_Expected_field_id_and_weathersummary_values_should_store_of_precip_prob_for_forecastday7_</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>78708b93-5053-425c-8401-c15acd1634cb</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/DataIngest/Iteris/Weather/DB_Fields_WeatherDailyHistorical_WeatherDailyForecast</testDataId>
      </testDataLink>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>917f8b71-c010-4e12-88c1-f53bb18ef5a9</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/DataIngest/Iteris/Weather/Forecast/Summary/DB_IterisRawData_Weather_Forecast_Summary</testDataId>
      </testDataLink>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>9c09bce0-8f11-4632-9655-cafc9d4113fc</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/DataIngest/Iteris/Weather/Forecast/Summary/Xls_Iteris_Weather_Forecast_Summary_RabbitMq_Messages</testDataId>
      </testDataLink>
   </testCaseLink>
   <testCaseLink>
      <guid>0e837066-54c8-4931-9242-0154f8b07ff0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/DataIngest/Iteris/Weather/Forecast/Summary/DI_W_SF_When_1_valid_field_Expected_field_id_and_weathersummary_values_should_store_of_precip_prob_for_today</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>e8578ea7-eb94-420a-8a1e-3737b646540c</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/DataIngest/Iteris/Weather/Forecast/Summary/DB_IterisRawData_Weather_Forecast_Summary</testDataId>
      </testDataLink>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>c404eb44-0b34-4dde-a416-8921f1984402</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/DataIngest/Iteris/Weather/Forecast/Summary/Xls_Iteris_Weather_Forecast_Summary_RabbitMq_Messages</testDataId>
      </testDataLink>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>0acf616e-46a7-4b66-9dab-1e375a879e20</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/DataIngest/Iteris/Weather/DB_Fields_WeatherDailyHistorical_WeatherDailyForecast</testDataId>
      </testDataLink>
   </testCaseLink>
   <testCaseLink>
      <guid>975e2af0-dfb3-46a9-9221-f6a257f80a75</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/DataIngest/Iteris/Weather/Forecast/Summary/DI_W_SF_When_1_valid_field_Expected_percentage_rainfall_for_next_1_day_should_store_in_fieldsummaryweatherforecast_table</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>7cf5d7f8-96cf-46e8-8b44-4de1333c0ab9</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/DataIngest/Iteris/Weather/DB_Fields_WeatherDailyHistorical_WeatherDailyForecast</testDataId>
      </testDataLink>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>f63801ed-e56c-4fcc-8ba4-fb0a7948c4bf</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/DataIngest/Iteris/Weather/Forecast/Summary/DB_IterisRawData_Weather_Forecast_Summary</testDataId>
      </testDataLink>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>6647bd7e-8e5b-4764-abb2-820bfe339dcc</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/DataIngest/Iteris/Weather/Forecast/Summary/Xls_Iteris_Weather_Forecast_Summary_RabbitMq_Messages</testDataId>
      </testDataLink>
   </testCaseLink>
   <testCaseLink>
      <guid>4ffdad90-e3d6-4729-91a0-73b899c80de1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/DataIngest/Iteris/Weather/Forecast/Summary/DI_W_SF_When_1_valid_field_Expected_percentage_rainfall_for_next_2_days_should_store_in_fieldsummaryweatherforecast_table</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>05a2372d-31c9-4d56-8d50-3238b69ec872</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/DataIngest/Iteris/Weather/Forecast/Summary/DB_IterisRawData_Weather_Forecast_Summary</testDataId>
      </testDataLink>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>5b9fbdf4-a1fe-4a8c-abe6-0b34f0b37d99</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/DataIngest/Iteris/Weather/Forecast/Summary/Xls_Iteris_Weather_Forecast_Summary_RabbitMq_Messages</testDataId>
      </testDataLink>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>d2686a87-d627-4515-87b5-95249f5f404c</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/DataIngest/Iteris/Weather/DB_Fields_WeatherDailyHistorical_WeatherDailyForecast</testDataId>
      </testDataLink>
   </testCaseLink>
   <testCaseLink>
      <guid>603a0f91-eaee-429b-b44d-a38e682b2d30</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/DataIngest/Iteris/Weather/Forecast/Summary/DI_W_SF_When_1_valid_field_Expected_percentage_rainfall_for_next_3_days_should_store_in_fieldsummaryweatherforecast_table</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>95c4c5db-0efd-44d2-ab02-63286e3b1a16</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/DataIngest/Iteris/Weather/Forecast/Summary/DB_IterisRawData_Weather_Forecast_Summary</testDataId>
      </testDataLink>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>a7376475-2e29-4df2-8a71-984aab3b6aac</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/DataIngest/Iteris/Weather/Forecast/Summary/Xls_Iteris_Weather_Forecast_Summary_RabbitMq_Messages</testDataId>
      </testDataLink>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>eebd4f5f-72bf-427e-9a82-8df43c79b1c0</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/DataIngest/Iteris/Weather/DB_Fields_WeatherDailyHistorical_WeatherDailyForecast</testDataId>
      </testDataLink>
   </testCaseLink>
   <testCaseLink>
      <guid>58df3d95-d0f9-4405-bd39-6b415a87c5b1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/DataIngest/Iteris/Weather/Forecast/Summary/DI_W_SF_When_1_valid_field_Expected_percentage_rainfall_for_next_4_days_should_store_in_fieldsummaryweatherforecast_table</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>06248bbc-eeb2-4119-9903-bc38b88b5266</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/DataIngest/Iteris/Weather/Forecast/Summary/DB_IterisRawData_Weather_Forecast_Summary</testDataId>
      </testDataLink>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>d02a1770-a974-44ff-b41c-378607e2fead</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/DataIngest/Iteris/Weather/Forecast/Summary/Xls_Iteris_Weather_Forecast_Summary_RabbitMq_Messages</testDataId>
      </testDataLink>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>f9b6dfd2-2683-4abb-ac78-982fd6e571fc</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/DataIngest/Iteris/Weather/DB_Fields_WeatherDailyHistorical_WeatherDailyForecast</testDataId>
      </testDataLink>
   </testCaseLink>
   <testCaseLink>
      <guid>3128088e-6c5e-41f7-a9e2-1380c5769f70</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/DataIngest/Iteris/Weather/Forecast/Summary/DI_W_SF_When_1_valid_field_Expected_percentage_rainfall_for_next_5_days_should_store_in_fieldsummaryweatherforecast_table</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>f7e30cc1-2907-41b4-8df0-7aee564048f2</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/DataIngest/Iteris/Weather/Forecast/Summary/DB_IterisRawData_Weather_Forecast_Summary</testDataId>
      </testDataLink>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>993f57c7-d040-4731-9414-0f07c9166d25</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/DataIngest/Iteris/Weather/Forecast/Summary/Xls_Iteris_Weather_Forecast_Summary_RabbitMq_Messages</testDataId>
      </testDataLink>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>ef28e1b0-446e-4ab0-b5a4-e7c68246e43e</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/DataIngest/Iteris/Weather/DB_Fields_WeatherDailyHistorical_WeatherDailyForecast</testDataId>
      </testDataLink>
   </testCaseLink>
   <testCaseLink>
      <guid>0a1981fd-946c-4eea-9adc-24d570a2cb90</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/DataIngest/Iteris/Weather/Forecast/Summary/DI_W_SF_When_1_valid_field_Expected_percentage_rainfall_for_next_6_days_should_store_in_fieldsummaryweatherforecast_table</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>7ff2c7e5-1c0b-4ff4-9191-13f457f76f36</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/DataIngest/Iteris/Weather/Forecast/Summary/DB_IterisRawData_Weather_Forecast_Summary</testDataId>
      </testDataLink>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>0c1c8617-ef8d-4385-ad44-68378e21d606</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/DataIngest/Iteris/Weather/Forecast/Summary/Xls_Iteris_Weather_Forecast_Summary_RabbitMq_Messages</testDataId>
      </testDataLink>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>76b7d61c-0403-4f08-96cc-8eb2e707b106</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/DataIngest/Iteris/Weather/DB_Fields_WeatherDailyHistorical_WeatherDailyForecast</testDataId>
      </testDataLink>
   </testCaseLink>
   <testCaseLink>
      <guid>969a0433-28eb-40ad-bba2-9621b0431045</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/DataIngest/Iteris/Weather/Forecast/Summary/DI_W_SF_When_1_valid_field_Expected_percentage_rainfall_for_next_7_days_should_store_in_fieldsummaryweatherforecast_table</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>cb16dd24-c5e8-41db-b3e8-7f1127177294</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/DataIngest/Iteris/Weather/Forecast/Summary/DB_IterisRawData_Weather_Forecast_Summary</testDataId>
      </testDataLink>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>cd9af506-edfe-4815-8663-31a66ec74bbc</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/DataIngest/Iteris/Weather/Forecast/Summary/Xls_Iteris_Weather_Forecast_Summary_RabbitMq_Messages</testDataId>
      </testDataLink>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>83dec3bd-df0f-4343-aba9-81af739e5069</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/DataIngest/Iteris/Weather/DB_Fields_WeatherDailyHistorical_WeatherDailyForecast</testDataId>
      </testDataLink>
   </testCaseLink>
   <testCaseLink>
      <guid>c9b6fb99-1efd-411e-9351-eec7abf747f7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/DataIngest/Iteris/Weather/Forecast/Summary/DI_W_SF_When_1_valid_field_Expected_percentage_rainfall_for_today_should_store_in_fieldsummaryweatherforecast</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>ac80c2a0-275a-445f-82f1-8af379e3bf5b</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/DataIngest/Iteris/Weather/DB_Fields_WeatherDailyHistorical_WeatherDailyForecast</testDataId>
      </testDataLink>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>456ae0fb-1965-49ae-a466-85c9b02e918a</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/DataIngest/Iteris/Weather/Forecast/Summary/DB_IterisRawData_Weather_Forecast_Summary</testDataId>
      </testDataLink>
   </testCaseLink>
   <testCaseLink>
      <guid>0423bae4-cdd5-4002-a1c1-f8e2bc5b7e1b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/DataIngest/Iteris/Weather/Forecast/Summary/DI_W_SF_When_field_id_is_empty_Expected_no_new_data_should_store_in_fieldsummaryweatherforecast_table</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>50925746-eef5-4b90-8723-ea99d7b14f95</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/DataIngest/Iteris/Weather/Forecast/Summary/Xls_Iteris_Weather_Forecast_Summary_RabbitMq_Messages</testDataId>
      </testDataLink>
   </testCaseLink>
   <testCaseLink>
      <guid>4a9cd169-9d35-426c-bd2a-4e8ce969944f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/DataIngest/Iteris/Weather/Forecast/Summary/DI_W_SF_When_have_1_valid_field_whitout_forecastday1_value_Expected_new_row_should_store_as_null_</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>2408252d-470e-4c36-a9e9-6ea43e51ae2a</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/DataIngest/Iteris/Weather/Forecast/Summary/DB_IterisRawData_Weather_Forecast_Summary</testDataId>
      </testDataLink>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>cc16cdf6-25f1-4f91-a848-a49f7012d625</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/DataIngest/Iteris/Weather/Forecast/Summary/Xls_Iteris_Weather_Forecast_Summary_RabbitMq_Messages</testDataId>
      </testDataLink>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>644eb6eb-a42c-427f-a067-a9cab5432d47</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/DataIngest/Iteris/Weather/DB_Fields_WeatherDailyHistorical_WeatherDailyForecast</testDataId>
      </testDataLink>
   </testCaseLink>
   <testCaseLink>
      <guid>9ec4e68f-0509-4edb-bd8d-f17f7983b6bc</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/DataIngest/Iteris/Weather/Forecast/Summary/DI_W_SF_When_have_1_valid_field_whitout_forecastday2_value_Expected_new_row_should_store_as_null_</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>c6b0fc5c-d6fc-41ab-be0f-d1b2e1469e4b</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/DataIngest/Iteris/Weather/Forecast/Summary/DB_IterisRawData_Weather_Forecast_Summary</testDataId>
      </testDataLink>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>1b429319-87ad-435a-8a82-5123f0a8b54d</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/DataIngest/Iteris/Weather/Forecast/Summary/Xls_Iteris_Weather_Forecast_Summary_RabbitMq_Messages</testDataId>
      </testDataLink>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>2ea72d2c-7332-4978-843d-8181821b2fb0</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/DataIngest/Iteris/Weather/DB_Fields_WeatherDailyHistorical_WeatherDailyForecast</testDataId>
      </testDataLink>
   </testCaseLink>
   <testCaseLink>
      <guid>6223d79b-41c2-4259-a102-6e82dd9dfac5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/DataIngest/Iteris/Weather/Forecast/Summary/DI_W_SF_When_have_1_valid_field_whitout_forecastday3_value_Expected_new_row_should_store_as_null_</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>64f3d17a-ef6f-4551-b11e-a538f9096e86</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/DataIngest/Iteris/Weather/Forecast/Summary/DB_IterisRawData_Weather_Forecast_Summary</testDataId>
      </testDataLink>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>d5c54149-b497-49c4-8243-86234c222e07</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/DataIngest/Iteris/Weather/Forecast/Summary/Xls_Iteris_Weather_Forecast_Summary_RabbitMq_Messages</testDataId>
      </testDataLink>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>4beb1f95-875a-4671-a8d5-88c7f7422b3f</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/DataIngest/Iteris/Weather/DB_Fields_WeatherDailyHistorical_WeatherDailyForecast</testDataId>
      </testDataLink>
   </testCaseLink>
   <testCaseLink>
      <guid>0e5c8f8e-83ec-44cd-be6d-ee20cc42e34d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/DataIngest/Iteris/Weather/Forecast/Summary/DI_W_SF_When_have_1_valid_field_whitout_forecastday4_value_Expected_new_row_should_store_as_null_</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>ac2eebf4-9710-4186-9ec9-1d7c6f489e20</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/DataIngest/Iteris/Weather/Forecast/Summary/DB_IterisRawData_Weather_Forecast_Summary</testDataId>
      </testDataLink>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>8b62acc9-f1ec-4a11-a139-454f7971b434</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/DataIngest/Iteris/Weather/Forecast/Summary/Xls_Iteris_Weather_Forecast_Summary_RabbitMq_Messages</testDataId>
      </testDataLink>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>7a16e49c-5188-436f-86f6-9953e65218f2</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/DataIngest/Iteris/Weather/DB_Fields_WeatherDailyHistorical_WeatherDailyForecast</testDataId>
      </testDataLink>
   </testCaseLink>
   <testCaseLink>
      <guid>379022d2-8ffa-4537-8711-9b38d4bc0a1c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/DataIngest/Iteris/Weather/Forecast/Summary/DI_W_SF_When_have_1_valid_field_whitout_forecastday5_value_Expected_new_row_should_store_as_null_</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>bd5b5c44-2540-428b-924f-882a76dec5b9</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/DataIngest/Iteris/Weather/Forecast/Summary/DB_IterisRawData_Weather_Forecast_Summary</testDataId>
      </testDataLink>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>3f1c4127-ea09-4e16-82a4-770935a27eb7</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/DataIngest/Iteris/Weather/Forecast/Summary/Xls_Iteris_Weather_Forecast_Summary_RabbitMq_Messages</testDataId>
      </testDataLink>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>2158e376-5a58-4c2a-a5fd-3a17a67c7cc9</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/DataIngest/Iteris/Weather/DB_Fields_WeatherDailyHistorical_WeatherDailyForecast</testDataId>
      </testDataLink>
   </testCaseLink>
   <testCaseLink>
      <guid>eacce638-33d6-4f41-900b-a3b1e96e1bfd</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/DataIngest/Iteris/Weather/Forecast/Summary/DI_W_SF_When_have_1_valid_field_whitout_forecastday6_value_Expected_new_row_should_store_as_null_</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>4c366de6-f2aa-403a-9234-2afe967ceb80</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/DataIngest/Iteris/Weather/Forecast/Summary/DB_IterisRawData_Weather_Forecast_Summary</testDataId>
      </testDataLink>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>2f17640e-15f9-4658-855b-b6c41fce4758</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/DataIngest/Iteris/Weather/Forecast/Summary/Xls_Iteris_Weather_Forecast_Summary_RabbitMq_Messages</testDataId>
      </testDataLink>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>56b01496-7267-4177-85ec-79f11d0606e6</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/DataIngest/Iteris/Weather/DB_Fields_WeatherDailyHistorical_WeatherDailyForecast</testDataId>
      </testDataLink>
   </testCaseLink>
   <testCaseLink>
      <guid>52620960-ee22-4ed9-8745-4337248c6d93</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/DataIngest/Iteris/Weather/Forecast/Summary/DI_W_SF_When_have_1_valid_field_whitout_forecastday7_value_Expected_new_row_should_store_as_null_</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>8397e4de-8b6d-4bb8-be2a-bf04f7fac0bd</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/DataIngest/Iteris/Weather/Forecast/Summary/DB_IterisRawData_Weather_Forecast_Summary</testDataId>
      </testDataLink>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>230ac9ee-bb80-4366-8a48-3e066a260479</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/DataIngest/Iteris/Weather/Forecast/Summary/Xls_Iteris_Weather_Forecast_Summary_RabbitMq_Messages</testDataId>
      </testDataLink>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>bf0feec4-0432-4cd1-9c12-8b2d3c3f247d</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/DataIngest/Iteris/Weather/DB_Fields_WeatherDailyHistorical_WeatherDailyForecast</testDataId>
      </testDataLink>
   </testCaseLink>
   <testCaseLink>
      <guid>1c97caf9-b3b2-4cd7-b4bf-2a34f1b6eeec</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/DataIngest/Iteris/Weather/Forecast/Summary/DI_W_SF_When_have_1_valid_field_whitout_today_value_Expected_new_row_should_store_as_null</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>dc3387c4-a1eb-473d-91a3-3370d058e0d4</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/DataIngest/Iteris/Weather/Forecast/Summary/DB_IterisRawData_Weather_Forecast_Summary</testDataId>
      </testDataLink>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>8572a4e6-6280-4447-9ee1-02cd50ef8a00</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/DataIngest/Iteris/Weather/Forecast/Summary/Xls_Iteris_Weather_Forecast_Summary_RabbitMq_Messages</testDataId>
      </testDataLink>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>1076dd62-79e0-41c5-86e1-cc60a03a9fce</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/DataIngest/Iteris/Weather/DB_Fields_WeatherDailyHistorical_WeatherDailyForecast</testDataId>
      </testDataLink>
   </testCaseLink>
   <testCaseLink>
      <guid>caf95d89-8f81-4928-a8ab-37cc181be497</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/DataIngest/Iteris/Weather/Forecast/Summary/DI_W_SF_When_precip_prob_for_forecastday1_is_lost_Expected_new_row_should_store_with_precip_prob_as_null_</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>4a797aff-5c4c-4d79-afcf-eb98656b6b11</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/DataIngest/Iteris/Weather/Forecast/Summary/DB_IterisRawData_Weather_Forecast_Summary</testDataId>
      </testDataLink>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>48be66d4-2143-4456-a1f7-c3290a73852e</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/DataIngest/Iteris/Weather/Forecast/Summary/Xls_Iteris_Weather_Forecast_Summary_RabbitMq_Messages</testDataId>
      </testDataLink>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>18b524b4-5514-4268-a066-b21d8ab58516</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/DataIngest/Iteris/Weather/DB_Fields_WeatherDailyHistorical_WeatherDailyForecast</testDataId>
      </testDataLink>
   </testCaseLink>
   <testCaseLink>
      <guid>42f1b213-2ae6-490d-a138-b48b27042d4c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/DataIngest/Iteris/Weather/Forecast/Summary/DI_W_SF_When_precip_prob_for_forecastday2_is_lost_Expected_new_row_should_store_with_precip_prob_as_null_</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>4d7e82f3-05c6-46fb-908a-2094d7f72f5f</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/DataIngest/Iteris/Weather/Forecast/Summary/DB_IterisRawData_Weather_Forecast_Summary</testDataId>
      </testDataLink>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>6ee4de49-e1b6-4d3e-973d-0f7444b73dec</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/DataIngest/Iteris/Weather/Forecast/Summary/Xls_Iteris_Weather_Forecast_Summary_RabbitMq_Messages</testDataId>
      </testDataLink>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>0c093ad0-fa9a-466a-aa4a-52084fe1a5b6</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/DataIngest/Iteris/Weather/DB_Fields_WeatherDailyHistorical_WeatherDailyForecast</testDataId>
      </testDataLink>
   </testCaseLink>
   <testCaseLink>
      <guid>a1175cec-7a08-48d5-9922-64b7c2477989</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/DataIngest/Iteris/Weather/Forecast/Summary/DI_W_SF_When_precip_prob_for_forecastday3_is_lost_Expected_new_row_should_store_with_precip_prob_as_null_</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>57267cba-fdbe-47cb-a1c4-04775001ad22</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/DataIngest/Iteris/Weather/Forecast/Summary/DB_IterisRawData_Weather_Forecast_Summary</testDataId>
      </testDataLink>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>287fce50-8dc0-4d5e-b5b4-4f929e5ae984</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/DataIngest/Iteris/Weather/Forecast/Summary/Xls_Iteris_Weather_Forecast_Summary_RabbitMq_Messages</testDataId>
      </testDataLink>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>d94a6698-65a0-4c96-a22f-086a512ee736</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/DataIngest/Iteris/Weather/DB_Fields_WeatherDailyHistorical_WeatherDailyForecast</testDataId>
      </testDataLink>
   </testCaseLink>
   <testCaseLink>
      <guid>3c9e30ed-5da6-49e1-afda-7937acde733e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/DataIngest/Iteris/Weather/Forecast/Summary/DI_W_SF_When_precip_prob_for_forecastday4_is_lost_Expected_new_row_should_store_with_precip_prob_as_null_</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>61347625-4a35-4e20-88af-215fd0559d31</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/DataIngest/Iteris/Weather/Forecast/Summary/DB_IterisRawData_Weather_Forecast_Summary</testDataId>
      </testDataLink>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>2b90693f-7fce-4ee3-b84a-242b73c424d8</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/DataIngest/Iteris/Weather/Forecast/Summary/Xls_Iteris_Weather_Forecast_Summary_RabbitMq_Messages</testDataId>
      </testDataLink>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>795dfa43-43f1-410a-857a-0321bf3d3729</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/DataIngest/Iteris/Weather/DB_Fields_WeatherDailyHistorical_WeatherDailyForecast</testDataId>
      </testDataLink>
   </testCaseLink>
   <testCaseLink>
      <guid>983c4f07-fb1a-4cff-80af-79ec5d576b45</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/DataIngest/Iteris/Weather/Forecast/Summary/DI_W_SF_When_precip_prob_for_forecastday5_is_lost_Expected_new_row_should_store_with_precip_prob_as_null_</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>4a01c352-dde0-4492-a6f7-35046cbf2744</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/DataIngest/Iteris/Weather/Forecast/Summary/DB_IterisRawData_Weather_Forecast_Summary</testDataId>
      </testDataLink>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>11857de8-a504-4db2-a95b-a260e0d92117</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/DataIngest/Iteris/Weather/Forecast/Summary/Xls_Iteris_Weather_Forecast_Summary_RabbitMq_Messages</testDataId>
      </testDataLink>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>b8421bde-7fb8-421a-9437-ec8173de0200</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/DataIngest/Iteris/Weather/DB_Fields_WeatherDailyHistorical_WeatherDailyForecast</testDataId>
      </testDataLink>
   </testCaseLink>
   <testCaseLink>
      <guid>1d85608c-1d57-4196-9b74-23f35f2270ed</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/DataIngest/Iteris/Weather/Forecast/Summary/DI_W_SF_When_precip_prob_for_forecastday6_is_lost_Expected_new_row_should_store_with_precip_prob_as_null_</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>2fa68bb8-13a0-428d-9f83-6ef6f3d93b65</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/DataIngest/Iteris/Weather/Forecast/Summary/DB_IterisRawData_Weather_Forecast_Summary</testDataId>
      </testDataLink>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>04031f08-4276-4444-a778-b715f47c12df</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/DataIngest/Iteris/Weather/Forecast/Summary/Xls_Iteris_Weather_Forecast_Summary_RabbitMq_Messages</testDataId>
      </testDataLink>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>98c6c22f-8cbf-4e83-8731-5797873e29f0</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/DataIngest/Iteris/Weather/DB_Fields_WeatherDailyHistorical_WeatherDailyForecast</testDataId>
      </testDataLink>
   </testCaseLink>
   <testCaseLink>
      <guid>b02eca5a-b807-4d55-b816-61f452df71ce</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/DataIngest/Iteris/Weather/Forecast/Summary/DI_W_SF_When_precip_prob_for_forecastday7_is_lost_Expected_new_row_should_store_with_precip_prob_as_null_</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>86109d6a-973e-4213-82b8-29e22f6a1ba8</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/DataIngest/Iteris/Weather/Forecast/Summary/DB_IterisRawData_Weather_Forecast_Summary</testDataId>
      </testDataLink>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>fb018c56-de26-45dc-a6cf-831a513ef7c2</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/DataIngest/Iteris/Weather/Forecast/Summary/Xls_Iteris_Weather_Forecast_Summary_RabbitMq_Messages</testDataId>
      </testDataLink>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>3c2952c2-3363-4099-9be8-ac5cdae9d2ec</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/DataIngest/Iteris/Weather/DB_Fields_WeatherDailyHistorical_WeatherDailyForecast</testDataId>
      </testDataLink>
   </testCaseLink>
   <testCaseLink>
      <guid>cc4ddd51-8e85-47dc-9eea-4a0c04705ff5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/DataIngest/Iteris/Weather/Forecast/Summary/DI_W_SF_When_precip_prob_for_today_is_lost_Expected_new_row_should_store_with_precip_prob_as_null</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>3e0bb54c-834d-4836-a787-d4482d32c9bf</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/DataIngest/Iteris/Weather/Forecast/Summary/DB_IterisRawData_Weather_Forecast_Summary</testDataId>
      </testDataLink>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>350fca2d-5459-48ca-b48e-e0e411fce43c</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/DataIngest/Iteris/Weather/DB_Fields_WeatherDailyHistorical_WeatherDailyForecast</testDataId>
      </testDataLink>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>d87491b5-7226-4242-8b8a-325887dd5b5a</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/DataIngest/Iteris/Weather/Forecast/Summary/Xls_Iteris_Weather_Forecast_Summary_RabbitMq_Messages</testDataId>
      </testDataLink>
   </testCaseLink>
   <testCaseLink>
      <guid>829d0d68-e547-488f-b12b-45e2dad1a8c9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/DataIngest/Iteris/Weather/Forecast/Summary/DI_W_SF_When_weather_database_is_empty_Expected_error_message_should_generate_and_process_should_not_broken</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>21b03bf0-923e-4711-be4e-975bd72af71e</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/DataIngest/Iteris/Weather/DB_Fields_WeatherDailyHistorical_WeatherDailyForecast</testDataId>
      </testDataLink>
   </testCaseLink>
</TestSuiteEntity>
