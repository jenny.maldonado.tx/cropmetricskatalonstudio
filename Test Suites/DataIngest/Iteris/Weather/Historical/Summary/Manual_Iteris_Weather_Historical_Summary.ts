<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Manual_Iteris_Weather_Historical_Summary</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>b9ccc1d3-008e-4777-9d7e-bc8101bafbee</testSuiteGuid>
   <testCaseLink>
      <guid>bd0a3688-258a-4bce-9e01-02b82d2ccd53</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/DataIngest/Iteris/Weather/Historical/Summary/DI_W_SH_When_1_field_id_as_char_Expected_new_data_should_not_store_in_fieldsummaryweatherhistorical_table</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>330e08ab-b809-4423-a730-409950e2c9cf</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/DataIngest/Iteris/Weather/Historical/Summary/Xls_Iteris_Weather_Historical_Summary_RabbitMq_Messages</testDataId>
      </testDataLink>
   </testCaseLink>
   <testCaseLink>
      <guid>f05cba54-d884-4af6-9ecb-498ea48739be</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/DataIngest/Iteris/Weather/Historical/Summary/DI_W_SH_When_1_field_id_is_not_in_iterisrawdata_Expected_new_data_should_not_store_in_fieldsummaryweatherhistorical_table</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>9faeb1af-b9bc-40ea-aaa2-5d75b0df64d5</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/DataIngest/Iteris/Weather/Historical/Summary/Xls_Iteris_Weather_Historical_Summary_RabbitMq_Messages</testDataId>
      </testDataLink>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>246a784a-388c-40a9-be75-40537f61e3ef</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/DataIngest/Iteris/Weather/Historical/Summary/DB_IterisRawData_Weather_Historical_Summary</testDataId>
      </testDataLink>
   </testCaseLink>
   <testCaseLink>
      <guid>71e63556-40b5-4209-8078-cbae0c4a47bf</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/DataIngest/Iteris/Weather/Historical/Summary/DI_W_SH_When_1_field_with_flag_isvalid_as_false_Expected_new_data_should_not_stored_in_fieldsummaryweatherhistorical_table</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>82d52e0b-4a88-4fe5-9c58-00021770cd3b</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/DataIngest/Iteris/Weather/DB_Fields_WeatherDailyHistorical_WeatherDailyForecast</testDataId>
      </testDataLink>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>bff56bfa-13c6-4728-80a2-35c633a3b565</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/DataIngest/Iteris/Weather/Historical/Summary/Xls_Iteris_Weather_Historical_Summary_RabbitMq_Messages</testDataId>
      </testDataLink>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>7a382a8d-fe07-4323-8340-7e3d6833aa63</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/DataIngest/Iteris/Weather/Historical/Summary/DB_IterisRawData_Weather_Historical_Summary</testDataId>
      </testDataLink>
   </testCaseLink>
   <testCaseLink>
      <guid>a28c3641-1e3d-4132-9b39-f524424d8637</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/DataIngest/Iteris/Weather/Historical/Summary/DI_W_SH_When_1_field_with_invalid_id_Expected_new_data_should_not_store_in_fieldsummaryweatherhistorical_table</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>d1843aea-450f-4739-aeb3-ffd3f20f66b0</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/DataIngest/Iteris/Weather/Historical/Summary/Xls_Iteris_Weather_Historical_Summary_RabbitMq_Messages</testDataId>
      </testDataLink>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>8915c6e7-3323-4927-82cb-242c05e9bbe7</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/DataIngest/Iteris/Weather/DB_Fields_WeatherDailyHistorical_WeatherDailyForecast</testDataId>
      </testDataLink>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>aee06acf-37e9-4c46-8997-b6d16ffca4a1</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/DataIngest/Iteris/Weather/Historical/Summary/DB_IterisRawData_Weather_Historical_Summary</testDataId>
      </testDataLink>
   </testCaseLink>
   <testCaseLink>
      <guid>3bfdc3e7-fe52-42ba-8cd6-a2368d3d0cdb</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/DataIngest/Iteris/Weather/Historical/Summary/DI_W_SH_When_1_valid_field_Expected_field_id_and_weathersummary_values_should_be_stored_of_precip_acc_period_yesterday</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>43be7f87-7b81-42b8-85f6-8b9f08bdbca9</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/DataIngest/Iteris/Weather/DB_Fields_WeatherDailyHistorical_WeatherDailyForecast</testDataId>
      </testDataLink>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>efd4f523-24d1-4bf2-920c-5fac6dfb1dd9</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/DataIngest/Iteris/Weather/Historical/Summary/Xls_Iteris_Weather_Historical_Summary_RabbitMq_Messages</testDataId>
      </testDataLink>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>357318b0-34d8-4eaf-83a9-33f701e82a0e</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/DataIngest/Iteris/Weather/Historical/Summary/DB_IterisRawData_Weather_Historical_Summary</testDataId>
      </testDataLink>
   </testCaseLink>
   <testCaseLink>
      <guid>83294cbd-0df0-4aff-93b5-84e4a0471775</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/DataIngest/Iteris/Weather/Historical/Summary/DI_W_SH_When_1_valid_field_Expected_yesterday_rainfall_should_store_in_fieldsummaryweatherhistorical_of_summarizeddata_database_</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>51d62a6f-93ae-467b-8226-17b5f0ed71dc</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/DataIngest/Iteris/Weather/DB_Fields_WeatherDailyHistorical_WeatherDailyForecast</testDataId>
      </testDataLink>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>cfcbf62c-496f-40dd-9225-6f8e08c3bb49</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/DataIngest/Iteris/Weather/Historical/Summary/Xls_Iteris_Weather_Historical_Summary_RabbitMq_Messages</testDataId>
      </testDataLink>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>4369d2f8-5658-45bf-bc31-e04cd2a85fff</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/DataIngest/Iteris/Weather/Historical/Summary/DB_IterisRawData_Weather_Historical_Summary</testDataId>
      </testDataLink>
   </testCaseLink>
   <testCaseLink>
      <guid>9bb77eca-a38c-4c1e-963d-3060c7aa1368</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/DataIngest/Iteris/Weather/Historical/Summary/DI_W_SH_When_field_id_is_empty_Expected_no_new_data_should_store_in_fieldsummaryweatherhistorical</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>5c82fc58-c3ce-4d42-8430-a63ac5a1b6e2</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/DataIngest/Iteris/Weather/Historical/Summary/Xls_Iteris_Weather_Historical_Summary_RabbitMq_Messages</testDataId>
      </testDataLink>
   </testCaseLink>
   <testCaseLink>
      <guid>f28dfcd6-944b-4abe-bc81-014f2c1938a6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/DataIngest/Iteris/Weather/Historical/Summary/DI_W_SH_When_weather_database_is_empty_Expected_error_message_should_be_generated_and_process_should_not_broken</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>a170c7fc-c8a6-452b-9298-f5e2c9c659b0</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/DataIngest/Iteris/Weather/Historical/Summary/Xls_Iteris_Weather_Historical_Summary_RabbitMq_Messages</testDataId>
      </testDataLink>
   </testCaseLink>
   <testCaseLink>
      <guid>a353f1a5-b97a-45cc-86ee-36be072de88e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/DataIngest/Iteris/Weather/Historical/Summary/DI_W_SH_When_1_field_id_as_char_Expected_new_data_should_not_store_in_fieldsummaryweatherhistorical_table</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>f105cd62-59d5-4869-889a-50a4549f1239</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/DataIngest/Iteris/Weather/Historical/Summary/Xls_Iteris_Weather_Historical_Summary_RabbitMq_Messages</testDataId>
      </testDataLink>
   </testCaseLink>
   <testCaseLink>
      <guid>55c98052-f0c8-45e5-807b-a1f06660337d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/DataIngest/Iteris/Weather/Historical/Summary/DI_W_SH_When_1_field_id_is_not_in_iterisrawdata_Expected_new_data_should_not_store_in_fieldsummaryweatherhistorical_table</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>dd4fdb91-745d-4f7e-a9bd-0cb1076c4806</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/DataIngest/Iteris/Weather/Historical/Summary/Xls_Iteris_Weather_Historical_Summary_RabbitMq_Messages</testDataId>
      </testDataLink>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>b27fdfd2-fc48-45b7-9c0d-e6703b79c18a</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/DataIngest/Iteris/Weather/DB_Fields_WeatherDailyHistorical_WeatherDailyForecast</testDataId>
      </testDataLink>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>068f3758-560c-4e3c-abd1-95e4b32e2ff1</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/DataIngest/Iteris/Weather/Historical/Summary/DB_IterisRawData_Weather_Historical_Summary</testDataId>
      </testDataLink>
   </testCaseLink>
   <testCaseLink>
      <guid>9035330e-8006-4026-a4b8-575de6c66e7b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/DataIngest/Iteris/Weather/Historical/Summary/DI_W_SH_When_1_field_with_flag_isvalid_as_false_Expected_new_data_should_not_stored_in_fieldsummaryweatherhistorical_table</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>0c6a3c88-cc33-4765-b1e7-88f41b76b70b</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/DataIngest/Iteris/Weather/DB_Fields_WeatherDailyHistorical_WeatherDailyForecast</testDataId>
      </testDataLink>
   </testCaseLink>
   <testCaseLink>
      <guid>61d44916-858b-4715-b6f1-8f933f348f17</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/DataIngest/Iteris/Weather/Historical/Summary/DI_W_SH_When_1_field_with_invalid_id_Expected_new_data_should_not_store_in_fieldsummaryweatherhistorical_table</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>506c0b6a-01fe-4617-ae87-96655729ecbb</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/DataIngest/Iteris/Weather/Historical/Summary/Xls_Iteris_Weather_Historical_Summary_RabbitMq_Messages</testDataId>
      </testDataLink>
   </testCaseLink>
   <testCaseLink>
      <guid>3e56ea51-5f79-481a-87c6-a0d78eb0655e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/DataIngest/Iteris/Weather/Historical/Summary/DI_W_SH_When_1_valid_field_Expected_field_id_and_weathersummary_values_should_be_stored_of_precip_acc_period_yesterday</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>2ae6cf88-4479-44e1-b3c1-4071fb51bae0</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/DataIngest/Iteris/Weather/DB_Fields_WeatherDailyHistorical_WeatherDailyForecast</testDataId>
      </testDataLink>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>24d7b5e9-6987-429a-ae8e-46c336816926</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/DataIngest/Iteris/Weather/Historical/Summary/Xls_Iteris_Weather_Historical_Summary_RabbitMq_Messages</testDataId>
      </testDataLink>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>d79c638d-e144-42ef-a89e-c170bea736e1</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/DataIngest/Iteris/Weather/Historical/Summary/Xls_Iteris_Weather_Historical_Summary_RabbitMq_Messages</testDataId>
      </testDataLink>
   </testCaseLink>
   <testCaseLink>
      <guid>231801ec-e434-419f-a5d9-78ee01867653</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/DataIngest/Iteris/Weather/Historical/Summary/DI_W_SH_When_1_valid_field_Expected_yesterday_rainfall_should_store_in_fieldsummaryweatherhistorical_of_summarizeddata_database_</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>31146ec7-cad0-4be0-8876-13a4ad2d5d56</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/DataIngest/Iteris/Weather/DB_Fields_WeatherDailyHistorical_WeatherDailyForecast</testDataId>
      </testDataLink>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>42973a62-5615-4b3b-ad2e-9644997ccf19</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/DataIngest/Iteris/Weather/Historical/Summary/Xls_Iteris_Weather_Historical_Summary_RabbitMq_Messages</testDataId>
      </testDataLink>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>2f613a57-bbb3-4abf-8151-c822d6ceb700</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/DataIngest/Iteris/Weather/Historical/Summary/DB_IterisRawData_Weather_Historical_Summary</testDataId>
      </testDataLink>
   </testCaseLink>
   <testCaseLink>
      <guid>21de322c-eb04-423f-83fd-009ca618354a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/DataIngest/Iteris/Weather/Historical/Summary/DI_W_SH_When_field_id_is_empty_Expected_no_new_data_should_store_in_fieldsummaryweatherhistorical</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>e5117483-e600-434b-b57a-40ce1d740320</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/DataIngest/Iteris/Weather/Historical/Summary/Xls_Iteris_Weather_Historical_Summary_RabbitMq_Messages</testDataId>
      </testDataLink>
   </testCaseLink>
   <testCaseLink>
      <guid>13d8c69a-cb1c-47f4-83e2-9331ec845074</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/DataIngest/Iteris/Weather/Historical/Summary/DI_W_SH_When_weather_database_is_empty_Expected_error_message_should_be_generated_and_process_should_not_broken</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>e2049bac-8298-4668-861b-d69899df5a68</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/DataIngest/Iteris/Weather/Historical/Summary/Xls_Iteris_Weather_Historical_Summary_RabbitMq_Messages</testDataId>
      </testDataLink>
   </testCaseLink>
   <testCaseLink>
      <guid>9a603e30-925e-4955-a23c-b504fa670e5f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/DataIngest/Iteris/Weather/Historical/Summary/DI_W_SH_When_precip_acc_period_for_yesterday_is_lost_Expected_new_row_should_store_as_precip_acc_period_null</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>2145ce47-b9e7-4f72-b5c9-7803407b324c</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/DataIngest/Iteris/Weather/DB_Fields_WeatherDailyHistorical_WeatherDailyForecast</testDataId>
      </testDataLink>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>b6e25cd2-119c-49f3-8a2c-37a2d4bc4adb</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/DataIngest/Iteris/Weather/Historical/Summary/Xls_Iteris_Weather_Historical_Summary_RabbitMq_Messages</testDataId>
      </testDataLink>
   </testCaseLink>
   <testCaseLink>
      <guid>82c6f542-c8a9-4522-8f70-72554feed154</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/DataIngest/Iteris/Weather/Historical/Summary/DI_W_SH_When_1_field_in_iterisrawdata_without_yesterday_values_Expected_new_row_should_store_as_yesterday_null_</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>3f2b25e1-701d-4760-b3f2-9bc961c0d14a</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/DataIngest/Iteris/Weather/DB_Fields_WeatherDailyHistorical_WeatherDailyForecast</testDataId>
      </testDataLink>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>70bd537e-5561-47ea-9c53-6d4fbc8cbace</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/DataIngest/Iteris/Weather/Historical/Summary/Xls_Iteris_Weather_Historical_Summary_RabbitMq_Messages</testDataId>
      </testDataLink>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>1894c77f-9668-40fe-845c-2704cdbc838a</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/DataIngest/Iteris/Weather/Historical/Summary/DB_IterisRawData_Weather_Historical_Summary</testDataId>
      </testDataLink>
   </testCaseLink>
</TestSuiteEntity>
