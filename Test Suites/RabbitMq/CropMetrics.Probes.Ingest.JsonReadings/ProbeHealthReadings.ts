<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>ProbeHealthReadings</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>b2ee65d4-6075-4574-9672-3ebfd9482e2c</testSuiteGuid>
   <testCaseLink>
      <guid>0dd895f1-dc05-452d-a643-dc5ab04cdf28</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RabbitMq/JsonReadings/ProbeHealthReadings/RMQ_CPJR_PHR_When_agsense_probe_has_valid_readings_Expected_status_code_200</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>415c4937-a0a2-42ae-8319-bf75301b3cc7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RabbitMq/JsonReadings/ProbeHealthReadings/RMQ_CPJR_PHR_When_agsense_probe_has_valid_readings_Expected_new_record_is_added_in_database_with_correct_data</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
