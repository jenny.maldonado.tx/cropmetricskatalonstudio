<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>SoilMoistureReadings</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>008290e7-973c-47b7-a213-4cd16d60f21d</testSuiteGuid>
   <testCaseLink>
      <guid>9841b3b2-3c1d-4c3c-b24a-453a8c8c16f2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RabbitMq/JsonReadings/SoilMoistureReadings/RMQ_CPJR_SMR_When_agsense_probe_has_valid_readings_Expected_status_code_200</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2678e6b4-c4fa-47a1-8520-d88295cbc962</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RabbitMq/JsonReadings/SoilMoistureReadings/RMQ_CPJR_SMR_When_ag_probe_has_valid_readings_Expected_new_record_is_added_in_database_with_correct_data</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>73cbb012-b99d-423f-bc58-ef66948babb0</variableId>
      </variableLink>
   </testCaseLink>
</TestSuiteEntity>
