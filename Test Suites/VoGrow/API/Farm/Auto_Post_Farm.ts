<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Auto_Post_Farm</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>188f6696-906b-456f-93cf-950b4879b591</testSuiteGuid>
   <testCaseLink>
      <guid>5f162408-ae77-4eba-90d4-c857ed2545e3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoGrow/API/Farm/Post/1 API_FARM_When_new_farm_is_created_as_grower_Expected_status_code_201</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>16abee2e-7dec-4652-ae6b-a19d5329c3af</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/VoGrow/DB_2Agent_2Growers</testDataId>
      </testDataLink>
   </testCaseLink>
   <testCaseLink>
      <guid>b41ddac0-759f-4f25-8272-b7e75d206a4c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoGrow/API/Farm/Post/2 API_FARM_When_new_farm_is_created_as_grower_Expected_new_farm_is_added_in_database</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8f33455a-c239-4c19-8668-517c9541552a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoGrow/API/Farm/Post/4 API_FARM_When_new_farm_is_created_as_dealer_for_one_of_the_grower_assigned_Expected_status_code_201</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>351b7c2d-452c-44d0-9a35-64e3b1f9d040</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoGrow/API/Farm/Post/4.1 API_FARM_When_new_farm_as_dealer_for_grower_not_assigned_Expected_status_code_400</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e83c0095-ae0e-449a-8ee1-474cf12f7133</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoGrow/API/Farm/Post/5 API_FARM_When_new_farm_as_dealer_for_one_grower_assigned_Expected_new_farm_is_added_in_database_only_for_corresponding_grower</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2527d6c9-b8d9-4f36-9d0a-e04755946232</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoGrow/API/Farm/Post/7 API_FARM_When_new_farm_as_dealer_for_one_grower_assigned_Expected_new_farm_is_not_added_in_database_for_dealer_account</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a5f8b531-b4e3-418e-aa3d-2529f6dc0f42</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoGrow/API/Farm/Post/8 API_FARM_When_new_farm_has_name_attribute_with_more_than_128_chars_Expected_status_code_400</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>16531449-b1b5-4577-bf94-36143404e100</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoGrow/API/Farm/Post/9 API_FARM_When_new_farm_has_name_attribute_with_more_than_128_chars_Expected_properly_error_message_is_displayed</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0a4e3cff-cbc7-4060-b679-f65dd50bb348</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoGrow/API/Farm/Post/10 API_FARM_When_new_farm_has_name_attribute_with_128_chars_Expected_status_code_201</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>fd1cd57f-9756-4ddc-a987-e7bf36e926ac</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoGrow/API/Farm/Post/11 API_FARM_When_new_farm_has_name_attribute_with_128_chars_Expected_new_farm_is_added_in_database</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2d52a72e-aac7-4fbd-a996-a8f35997e63e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoGrow/API/Farm/Post/12 API_FARM_When_new_farm_has_name_attribute_with_only_spaces_Expected_status_code_400</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>560b4a96-bab6-40bc-9356-003e0fa4b69d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoGrow/API/Farm/Post/12.1 API_FARM_When_new_farm_has_name_attribute_with_only_spaces_Expected_properly_error_message_is_displayed</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b275e459-76bc-41ac-9e00-586ee0de41cf</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoGrow/API/Farm/Post/13 API_FARM_When_new_farm_has_name_attribute_as_empty_Expected_status_code_400</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>345b66c1-af5a-48c1-b307-11520dcd6ab8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoGrow/API/Farm/Post/14 API_FARM_When_new_farm_has_name_attribute_as_empty_Expected_properly_error_message_is_displayed</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>09e1dffb-ae7a-4558-ac9d-5da6efa3ea8c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoGrow/API/Farm/Post/15 API_FARM_When_new_farm_has_name_attribute_as_symbols_Expected_status_code_400</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a4c45821-1b4e-444c-a372-eda0a4c479b4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoGrow/API/Farm/Post/16 API_FARM_When_new_farm_has_name_attribute_as_symbols_Expected_properly_error_message_is_displayed</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b325d583-65b5-42c3-aa1e-09d9be77b679</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoGrow/API/Farm/Post/17 API_FARM_When_new_farm_has_name_attribute_with_spaces_and_chars_Expected_status_code_201</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c65ba707-d90f-4141-acc0-984b80bc4e54</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoGrow/API/Farm/Post/18 API_FARM_When_new_farm_has_name_attribute_with_only_numbers_Expected_status_code_201</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a1c5e706-5a6b-47d1-86d3-d73e8d93a868</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoGrow/API/Farm/Post/19 API_FARM_When_new_farm_has_name_attribute_with_only_chars_Expected_status_code_201</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>119604e0-7c55-4a92-97be-2365742f414d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoGrow/API/Farm/Post/20 API_FARM_When_new_farm_has_an_existing_name_for_the_user_token_Expected_status_code_400</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c0584f83-bc57-4a20-9862-f6b76b1ad92c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoGrow/API/Farm/Post/21 API_FARM_When_new_farm_has_an_existing_name_for_the_user_token_Expected_properly_error_message_is_displayed</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>39c07bed-3b98-4198-9cd7-0a0bf2610475</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoGrow/API/Farm/Post/22 API_FARM_When_new_farm_has_the_name_of_the_other_user_different_of_the_user_token_Expected_status_code_201</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>88865021-6e26-4427-9867-a41b5628c85f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoGrow/API/Farm/Post/23 API_FARM_When_new_farm_has_the_userid_attribute_as_empty_Expected_status_code_400</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>44992835-c0bd-4b45-8899-8db35340a91a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoGrow/API/Farm/Post/24 API_FARM_When_new_farm_has_the_userid_attribute_as_empty_Expected_properly_error_message_is_displayed</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>67768cb5-ce8d-4efd-b3a9-feba6c126ed0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoGrow/API/Farm/Post/25 API_FARM_When_new_farm_has_the_userid_attribute_with_chars_Expected_status_code_400</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0886f6c8-c68f-4384-abf3-bbb1ece254bc</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoGrow/API/Farm/Post/26 API_FARM_When_new_farm_has_the_userid_attribute_with_chars_Expected_properly_error_message_is_displayed</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3d528b39-5a92-4075-a016-54c7a3203d98</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoGrow/API/Farm/Post/27 API_FARM_When_new_farm_has_the_userid_attribute_with_spaces_Expected_status_code_400</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b081150c-c4c7-42ef-87ec-555179e29b80</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoGrow/API/Farm/Post/28 API_FARM_When_new_farm_has_the_userid_attribute_with_spaces_Expected_properly_error_message_is_displayed</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>33204e39-4f3e-43e4-929b-ddaf72aa1188</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoGrow/API/Farm/Post/29 API_FARM_When_new_farm_has_the_valid_userid_attribute_but_different_of_the_user_token_Expected_status_code_400</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c1afe596-6eca-4141-adc2-784dce0f3c0b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoGrow/API/Farm/Post/30 API_FARM_When_new_farm_has_valid_userid_attribute_but_different_of_user_token_Expected_properly_error_message_is_displayed</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>74dbc7de-5c0b-42ff-a819-ba1e1c429728</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoGrow/API/Farm/Post/31 API_FARM_When_new_farm_has_an_invalid_userid_attribute_Expected_status_code_400</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>56b2c17a-b191-46b3-a316-3560653d33fe</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoGrow/API/Farm/Post/32 API_FARM_When_new_farm_has_an_invalid_userid_attribute_Expected_properly_error_message_is_displayed</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9e1a6546-4525-4fc0-a013-780abd01bc8f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoGrow/API/Farm/Post/33 API_FARM_When_new_farm_is_created_as_dealer_read_only_for_one_of_the_grower_assigned_Expected_status_code_201</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
