<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Auto_Put_Farm</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>e46e82f8-3e0c-4c09-a08c-da3daa1990f7</testSuiteGuid>
   <testCaseLink>
      <guid>26366394-d34e-43d4-af11-9e7b242ac714</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoGrow/API/Farm/Put/1 API_FARM_When_farm_is_updated_as_grower_Expected_status_code_200</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f4de85bc-6efb-4472-8ea6-f8886661311e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoGrow/API/Farm/Put/2 API_FARM_When_farm_is_updated_as_grower_Expected_new_farm_is_updated_in_database</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9c587476-2d13-442a-83f1-96a79f0134e9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoGrow/API/Farm/Put/4 API_FARM_When_farm_is_updated_as_dealer_for_one_of_the_grower_assigned_Expected_status_code_201</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>dbe918ac-59e1-4e6a-bbec-dd24ff4b0a9b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoGrow/API/Farm/Put/4.1 API_FARM_When_farm_updated_as_dealer_for_grower_not_assigned_Expected_status_code_400</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>dcea4a3c-f159-400c-bb73-64ce5acc7beb</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoGrow/API/Farm/Put/4.2 API_FARM_When_farm_updated_as_dealer_for_grower_not_assigned_Expected_properly_error_message_is_displayed</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>262cf825-fedd-4df9-b68c-be58370e72df</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoGrow/API/Farm/Put/5 API_FARM_When_farm_updated_as_dealer_for_one_grower_assigned_Expected_new_farm_is_added_in_database_only_for_corresponding_grower</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>fbdfa1ed-fdac-4c81-97b7-b5b7443c1fa1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoGrow/API/Farm/Put/7 API_FARM_When_farm_updated_as_dealer_for_one_grower_assigned_Expected_new_farm_is_not_added_in_database_for_dealer_account</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>684b519b-e29b-4d3f-9f37-194e2ac493d0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoGrow/API/Farm/Put/8 API_FARM_When_farm_updated_has_name_attribute_with_more_than_128_chars_Expected_status_code_400</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8e4b8a65-2405-432f-866e-25aad99073ca</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoGrow/API/Farm/Put/9 API_FARM_When_farm_updated_has_name_attribute_with_more_than_128_chars_Expected_properly_error_message_is_displayed</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d9814675-ed9d-4636-97ac-7566b3125fbc</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoGrow/API/Farm/Put/10 API_FARM_When_new_farm_has_name_attribute_with_128_chars_Expected_status_code_201</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>91c87fe3-36bf-422d-9e59-e2027dc43dc4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoGrow/API/Farm/Put/11 API_FARM_When_farm_updated_has_name_attribute_with_128_chars_Expected_new_farm_is_added_in_database</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3df81c11-d1e5-4e49-95f1-90b61c79c9d9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoGrow/API/Farm/Put/12 API_FARM_When_farm_updated_has_name_attribute_with_only_spaces_Expected_status_code_400</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e5a0a0c6-28fd-44d6-9e79-1ea5a03c217f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoGrow/API/Farm/Put/12.1 API_FARM_When_farm_updated_has_name_attribute_with_only_spaces_Expected_properly_error_message_is_displayed</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>dd2f7549-cc43-4bb4-9f40-80d54423b4f8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoGrow/API/Farm/Put/13 API_FARM_When_farm_updated_has_name_attribute_as_empty_Expected_status_code_400</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b1d4adbc-f384-4cd4-bcbd-534b7e79df05</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoGrow/API/Farm/Put/14 API_FARM_When_farm_updated_has_name_attribute_as_empty_Expected_properly_error_message_is_displayed</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>fbb154fe-1424-4391-a9fc-733465aa6f77</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoGrow/API/Farm/Put/15 API_FARM_When_farm_updated_has_name_attribute_as_symbols_Expected_status_code_400</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1d73f5ef-305d-4498-95c9-c50e90a5be5b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoGrow/API/Farm/Put/16 API_FARM_When_farm_updated_has_name_attribute_as_symbols_Expected_properly_error_message_is_displayed</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>42111901-c04c-4196-9350-26f50fc1fd9d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoGrow/API/Farm/Put/17 API_FARM_When_farm_updated_has_name_attribute_with_spaces_and_chars_Expected_status_code_201</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d81bdb33-400a-44d1-9338-87d910486427</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoGrow/API/Farm/Put/18 API_FARM_When_farm_updated_has_name_attribute_with_only_numbers_Expected_status_code_201</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>365f2da3-7d7f-411e-9fc6-a6dbf82621ad</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoGrow/API/Farm/Put/19 API_FARM_When_farm_is_updated_with_name_attribute_as_only_chars_Expected_status_code_201</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e43cced3-7465-42ed-9fec-5ea153399bc0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoGrow/API/Farm/Put/20 API_FARM_When_farm_updated_has_an_existing_name_for_the_user_token_Expected_status_code_400</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5f452560-593b-4b90-9910-2485beb62a8d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoGrow/API/Farm/Put/21 API_FARM_When_farm_updated_has_an_existing_name_for_the_user_token_Expected_properly_error_message_is_displayed</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ecf19ae8-d326-4751-b7d1-aee09d49b66e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoGrow/API/Farm/Put/22 API_FARM_When_farm_updated_has_the_name_of_the_other_user_different_of_the_user_token_Expected_status_code_201</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>96408218-210d-42a7-907e-b2686a95a31e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoGrow/API/Farm/Put/23 API_FARM_When_farm_updated_has_the_farmid_parameter_as_empty_Expected_status_code_405</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>11d06d89-3925-4543-afaa-ec5735b96aca</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoGrow/API/Farm/Put/24 API_FARM_When_farm_updated_has_the_farmid_parameter_as_empty_Expected_properly_error_message_is_displayed</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>831d6997-e4cc-4154-b6cb-6cee7bd34b94</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoGrow/API/Farm/Put/25 API_FARM_When_farm_updated_has_the_farmid_parameter_with_chars_Expected_status_code_404</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>82aba2c4-ca2e-4540-8ad4-f5d9b950c022</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoGrow/API/Farm/Put/27 API_FARM_When_farm_updated_has_the_farmid_paramert_with_spaces_Expected_status_code_400</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8b5fc50d-c516-4690-a872-0d9807d1e723</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoGrow/API/Farm/Put/29 API_FARM_When_farm_updated_has_the_valid_farmid_parameter_but_different_of_the_user_token_Expected_status_code_400</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2c165d90-013f-443e-95b0-29d1c507eab2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoGrow/API/Farm/Put/30 API_FARM_When_farm_updated_has_valid_farmid_parameter_but_different_of_user_token_Expected_properly_error_message_is_displayed</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7cdeb2c6-bc38-45f0-a841-55a96caee8bd</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoGrow/API/Farm/Put/31 API_FARM_When_farm_updated_has_an_invalid_farmid_parameter_Expected_status_code_400</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5a5f8d0e-02e9-4b01-acfc-5ae7d575bc23</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoGrow/API/Farm/Put/32 API_FARM_When_farm_updated_has_an_invalid_farmid_parameter_Expected_properly_error_message_is_displayed</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a9140381-d7c6-4d4d-a1be-606ebf379d2b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoGrow/API/Farm/Put/33 API_FARM_When_farm_is_updated_as_dealer_read_only_for_one_of_the_grower_assigned_Expected_status_code_201</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
