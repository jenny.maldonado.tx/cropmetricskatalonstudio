<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Auto_User</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>73da685d-d7af-469f-bc33-14898bc4dd9d</testSuiteGuid>
   <testCaseLink>
      <guid>5bb9eb1f-708d-48a0-a5ef-ba34efb7b079</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoGrow/API/User/Info/1 API_USER_When_user_is_client_only_Expected_status_code_200</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>574d32bb-4227-4a29-8226-3c93170170af</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoGrow/API/User/Info/2 API_USER_When_user_is_client_only_Expected_response_body_content_with_the_conresponding_grower_data</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>afa6ff32-290b-43ef-8b98-17d4e2f91758</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoGrow/API/User/Info/3 API_USER_When_user_is_agent_only_Expected_status_code_200</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9b5e11ac-624f-435e-8f0d-33f63f3a1b38</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoGrow/API/User/Info/4 API_USER_When_user_is_agent_only_Expected_response_body_content_with_the_conresponding_agent_data</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>87f4cfe9-814c-4a61-a043-035c70d6afe3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoGrow/API/User/Info/5 API_USER_When_user_token_is_wrong_Expected_status_code_401</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>31b25233-6d4b-49e1-9454-8771f72bf4f7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoGrow/API/User/Info/6 API_USER_When_user_token_is_wrong_Expected_properly_error_message_is_displayed</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e97368fc-9e62-494f-aedb-2eff6d9b8b22</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoGrow/API/User/Info/7 API_USER_When_user_has_admin_role_Expected_status_code_401</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a8dda13d-8236-4380-a8d1-9b43497de076</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoGrow/API/User/Info/8 API_USER_When_user_has_admin_role_Expected_properly_error_message_is_displayed</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
