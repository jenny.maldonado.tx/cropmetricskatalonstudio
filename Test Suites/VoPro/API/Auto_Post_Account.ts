<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Auto_Post_Account</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>028ab569-00fc-481e-ad9e-bab64136e842</testSuiteGuid>
   <testCaseLink>
      <guid>0ffe308f-c742-44f6-a817-a3ea0e1e7efd</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoPro/API/Account/PostUsers/API_ACNT_When_new_account_is_created_Expected_status_code_200</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>2d8db853-1ceb-45c8-b797-ebd547f03702</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>71d5706f-3219-40eb-ad8c-1c04e53a4300</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>6c52d124-b58d-4bdf-af69-b376e3afea9c</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>d28b2dff-1a27-4f07-9bbc-254faeb9fcb6</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>68199d6c-731a-4837-b127-7b8dea9b374d</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>3a795841-5de7-4055-96e0-13b08cb11ba9</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>9db957fb-3df8-41b5-8e6d-59d7bedb5537</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>863baabf-cb44-49b5-9397-9de3a389b4fc</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>1478b58e-4ed4-49e8-945e-b8293d67a4f3</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>4a67d647-d76e-4298-a669-f8daaab2e062</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>ec5cb517-c793-4c5b-9e32-0d0c6efe6ab0</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>4d9de205-9dcd-439d-909a-dbfac061d088</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>36c1cb96-5965-47f4-ac6a-8587ac913243</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoPro/API/Account/PostUsers/API_ACNT_When_new_account_is_created_Expected_new_account_is_added_in_database</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>6e98ea4d-1c4b-407e-a48a-3fd9cd8eaa5b</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>e9ddfc7e-a400-4367-ad6e-58150731b812</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>8021cabc-d2d4-4fe2-bd02-2bec042cda96</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>57af7815-6ffc-42c9-86f0-f9d2bf836c0f</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>326d1493-5a58-46eb-b920-fee775065c33</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>773cfdf9-4d1b-4a9a-bb55-1919921193c3</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>f8d1867c-7fe3-4257-ba4f-0b8e523a5fe8</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>45948aa5-c4b8-400e-8ff6-a1d47b413663</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>0d09b3ac-cafa-46c3-97fb-e1b9a17c46cc</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>d00342cd-4ec9-41ec-9521-58011b9a0935</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>ab91e061-c5e6-4389-8a75-15d9aa71c0c4</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>bf17dbbb-041c-432b-a532-0db87c76327a</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>0c83c4e6-f2c2-4504-ab95-920d5c745634</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>83e62a6b-037b-4fd0-b534-edcf9c6c1a1a</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>faed23b3-4034-4696-b07b-a044ffb34bd5</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>6dc8167c-23c7-4902-91e1-800a6c54cdd2</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>55f64983-ee17-4e6e-a4e0-0eb1a807f207</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoPro/API/User/API_USER_When_account_is_deleted_Expected_status_code_200</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>abf2df7d-c5d0-4257-8660-35fdf39d416d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoPro/API/User/API_USER_When_account_is_deleted_Expected_isvalid_flag_updated_to_false_in_database</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2058f946-f31f-4b0e-b778-7f550d17be58</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoPro/API/Account/PostUsers/API_ACNT_When_try_to_create_a_new_user_without_password_attribute_Expected_status_code_400</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ba02a759-e5a4-4ac7-a248-d078ab28d0e3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoPro/API/Account/PostUsers/API_ACNT_When_try_to_create_a_new_user_without_email_attribute_Expected_status_code_400</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f58ceabc-66ba-4878-8ff6-638ca4ba51d8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoPro/API/Account/PostUsers/API_ACNT_When_try_to_create_a_new_user_without_fullname_attribute_Expected_status_code_400</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c9be62e5-67a9-42e4-b7e4-768f81b8ec24</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoPro/API/Account/PostUsers/API_ACNT_When_try_to_create_a_new_user_without_phone_attribute_Expected_status_code_200</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9913f3dc-1f3a-4160-8b4d-63e509d6bf24</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoPro/API/Account/PostUsers/API_ACNT_When_try_to_create_a_new_user_with_phone_attribute_as_char_Expected_status_code_200</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>55f64d7a-8b4d-4127-9659-fb095e4d7f94</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoPro/API/Account/PostUsers/API_ACNT_When_try_to_create_a_new_user_with_phone_attribute_as_spaces_Expected_status_code_200</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5cbb4438-a758-4dd2-be3f-6e361ddca197</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoPro/API/Account/PostUsers/API_ACNT_When_try_to_create_a_new_user_without_dealer_attribute_Expected_status_code_200</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>59ecfd0f-6ffd-475f-b349-786ce287e110</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoPro/API/Account/PostUsers/API_ACNT_When_try_to_create_a_new_user_without_addressline1_attribute_Expected_status_code_200</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6c4667bd-b779-4820-90b7-e039f1aeb0cb</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoPro/API/Account/PostUsers/API_ACNT_When_try_to_create_a_new_user_without_addressline2_attribute_Expected_status_code_200</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0d3c13ca-3880-443d-8d69-40d2dc443484</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoPro/API/Account/PostUsers/API_ACNT_When_try_to_create_a_new_user_without_town_attribute_Expected_status_code_200</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c214401f-7db7-4135-8c1b-38d69250ef02</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoPro/API/Account/PostUsers/API_ACNT_When_try_to_create_a_new_user_without_state_attribute_Expected_status_code_200</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1ae9d155-7620-42ac-bb1c-a9b74e77dc88</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoPro/API/Account/PostUsers/API_ACNT_When_try_to_create_a_new_user_without_zip_attribute_Expected_status_code_200</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>19d77569-b66f-4dd1-a0cf-398a90910135</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoPro/API/Account/PostUsers/API_ACNT_When_try_to_create_a_new_user_without_country_attribute_Expected_status_code_200</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>11939998-60a9-4123-9910-9a3ac914aa05</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoPro/API/Account/PostUsers/API_ACNT_When_try_to_create_a_new_user_without_roles_attribute_Expected_status_code_400</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>42471414-3d3a-4c1e-a5f8-be9a84a188a2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoPro/API/Account/PostUsers/API_ACNT_When_new_account_have_less_6_characters_in_password_attribute_Expected_status_code_400</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>cd6eded2-a063-4197-ba70-9ef7c7b00c30</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoPro/API/Account/PostUsers/API_ACNT_When_new_account_have_more_15_characters_in_password_attribute_Expected_status_code_400</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c15e4bae-bd64-46bd-aaf3-6459c0e9ba47</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoPro/API/Account/PostUsers/API_ACNT_When_new_account_have_15_characters_in_password_attribute_Expected_status_code_200</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1c52a04c-fa8d-4946-966d-9da689b2885d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoPro/API/Account/PostUsers/API_ACNT_When_new_account_have_spaces_in_password_attribute_Expected_status_code_400</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>acdb22ab-5eea-4a0e-978d-4194600bfd90</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoPro/API/Account/PostUsers/API_ACNT_When_new_account_have_only_spaces_in_password_attribute_Expected_status_code_400</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8cd8933f-0da7-4abd-aaf9-72897e17ab3a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoPro/API/Account/PostUsers/API_ACNT_When_new_account_have_symbols_in_password_attribute_Expected_status_code_200</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3a980e70-32a7-4558-b209-7813599834c4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoPro/API/Account/PostUsers/API_ACNT_When_new_account_have_6_characters_in_password_attribute_Expected_status_code_200</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>bd43a2c5-2a2d-4a22-8ec5-45efa03a0b7a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoPro/API/Account/PostUsers/API_ACNT_When_new_account_have_an_existing_email_value_in_email_attribute_Expected_status_code_400</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>61f2f704-ceb4-4ccd-b31e-b8a032f2e5bb</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoPro/API/Account/PostUsers/API_ACNT_When_new_account_have_wrong_format_in_email_attribute_Expected_status_code_200</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>03987c0b-d116-47db-bbcf-3677b89478be</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoPro/API/Account/PostUsers/API_ACNT_When_new_account_have_spaces_in_email_attribute_Expected_status_code_200</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c286b677-5237-4385-adc7-d1f894aa17e1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoPro/API/Account/PostUsers/API_ACNT_When_new_account_have_symbols_in_email_attribute_Expected_status_code_400</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>56247ddb-bfea-4c46-a531-f6a4dcef5fc1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoPro/API/Account/PostUsers/API_ACNT_When_new_account_have_long_fullname_attribute_Expected_status_code_200</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e5ab4598-9724-476c-a071-ade3cb3a8567</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoPro/API/Account/PostUsers/API_ACNT_When_new_account_have_symbols_in_phone_attribute_Expected_status_code_200</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ea06f42d-7ecf-4fe2-8e11-cd40246e266d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoPro/API/Account/PostUsers/API_ACNT_When_new_account_have_long_dealer_attribute_Expected_status_code_200</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4fa16200-360d-409f-92b9-7684c3f2f777</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoPro/API/Account/PostUsers/API_ACNT_When_new_account_have_symbols_in_dealer_attribute_Expected_status_code_200</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a16aff02-7d45-4b9c-8b11-101f944c30f1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoPro/API/Account/PostUsers/API_ACNT_When_new_account_have_long_addressline1_attribute_Expected_status_code_200</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8a71716e-5ed7-4c65-a08b-a209e287c71e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoPro/API/Account/PostUsers/API_ACNT_When_new_account_have_symbols_in_addressline1_attribute_Expected_status_code_200</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>895df3e2-607a-4d66-b4cc-6ccafdf6054e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoPro/API/Account/PostUsers/API_ACNT_When_new_account_have_long_addressline2_attribute_Expected_status_code_200</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>81543099-f211-455b-8303-c7ef3e685283</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoPro/API/Account/PostUsers/API_ACNT_When_new_account_have_symbols_in_addressline2_attribute_Expected_status_code_200</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e1b6a0ea-4504-49ef-9c7c-dcc5bba41813</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoPro/API/Account/PostUsers/API_ACNT_When_new_account_have_long_town_attribute_Expected_status_code_200</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>da44bcae-c493-4ec2-8e0e-5bdd36ad846c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoPro/API/Account/PostUsers/API_ACNT_When_new_account_have_symbols_in_town_attribute_Expected_status_code_200</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>fdda94c8-dcda-4dcb-8462-09475a57a26c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoPro/API/Account/PostUsers/API_ACNT_When_new_account_have_long_state_attribute_Expected_status_code_200</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c1873056-af3e-40a0-b901-532ad519508a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoPro/API/Account/PostUsers/API_ACNT_When_new_account_have_spaces_in_state_attribute_Expected_status_code_200</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c676da44-a871-4db8-9835-917653a1fd65</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoPro/API/Account/PostUsers/API_ACNT_When_new_account_have_symbols_in_state_attribute_Expected_status_code_200</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6e14a43c-7bb5-46f4-a09b-c2c5d3cb6ced</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoPro/API/Account/PostUsers/API_ACNT_When_new_account_have_long_zip_attribute_Expected_status_code_200</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2fcd937d-4c1e-4a04-a305-54c41e9272d6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoPro/API/Account/PostUsers/API_ACNT_When_new_account_have_chars_zip_attribute_Expected_status_code_200</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0db4a205-64a5-4398-a761-23011b7b8840</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoPro/API/Account/PostUsers/API_ACNT_When_new_account_have_spaces_in_zip_attribute_Expected_status_code_200</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>348ca53f-950f-4294-8614-ca4b52eaf2e1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoPro/API/Account/PostUsers/API_ACNT_When_new_account_have_symbols_in_zip_attribute_Expected_status_code_200</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>64e1c30c-82a2-4afa-bc9c-ef99a1cbafb2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoPro/API/Account/PostUsers/API_ACNT_When_new_account_have_number_in_zip_attribute_Expected_status_code_200</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f2f4aac2-1405-4fb0-8803-cca677ef4d61</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoPro/API/Account/PostUsers/API_ACNT_When_new_account_have_long_country_attribute_Expected_status_code_200</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>984ff42b-a193-4ae6-9ae9-fae3752b01d3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoPro/API/Account/PostUsers/API_ACNT_When_new_account_have_number_country_attribute_Expected_status_code_200</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5963c0ee-cf55-4095-b3c3-6a79a7046340</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoPro/API/Account/PostUsers/API_ACNT_When_new_account_have_spaces_in_country_attribute_Expected_status_code_200</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>27d8a71e-c16b-466d-b4c0-55990e3234c7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoPro/API/Account/PostUsers/API_ACNT_When_new_account_have_symbols_in_country_attribute_Expected_status_code_200</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>cef25112-d6d7-41e1-a848-d23cf82c450f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoPro/API/Account/PostUsers/API_ACNT_When_new_account_have_invalid_roles_attribute_Expected_status_code_400</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3f61eb55-123c-4589-9f6e-459086d325a6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoPro/API/Account/PostUsers/API_ACNT_When_new_account_have_number_in_roles_attribute_Expected_status_code_400</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>af2bf29d-89ae-4c3b-a933-66cb7afbe7dd</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoPro/API/Account/PostUsers/API_ACNT_When_new_account_have_spaces_in_roles_attribute_Expected_status_code_200</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9efb4a1b-50ac-488b-81a3-dbff2fcd3826</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoPro/API/Account/PostUsers/API_ACNT_When_new_account_have_symbols_in_roles_attribute_Expected_status_code_200</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a34c63a9-31ce-44fb-91a9-8b8cce30160b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoPro/API/Account/PostUsers/API_ACNT_When_new_account_have_roles_attribute_as_client_Expected_status_code_200</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b50a1ab2-317d-4c11-b194-3a75978ba4a2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoPro/API/Account/PostUsers/API_ACNT_When_new_client_account_is_created_Expected_new_account_is_added_in_db_with_correct_roleid</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3418f7ca-1fb6-4597-94cd-6f7bdcef7c2e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoPro/API/Account/PostUsers/API_ACNT_When_new_account_have_roles_attribute_as_administrator_Expected_status_code_200</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e332a6fd-9015-443d-87ab-e66a91129184</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoPro/API/Account/PostUsers/API_ACNT_When_new_administrator_account_is_created_Expected_new_account_is_added_in_db_with_correct_roleid</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2c427ca5-8fa3-4e6a-8345-130195d7cacd</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoPro/API/Account/PostUsers/API_ACNT_When_new_account_have_roles_attribute_as_agent_Expected_status_code_200</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f62c63d3-e942-4cba-b87d-174e572e9b40</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoPro/API/Account/PostUsers/API_ACNT_When_new_agent_account_is_created_Expected_new_account_is_added_in_db_with_correct_roleid</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2a18ae1a-ba1d-4a9f-8bd7-4457e886ede2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoPro/API/Account/PostUsers/API_ACNT_When_new_account_have_roles_attribute_as_fieldcrew_Expected_status_code_200</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>43e43ffa-40e8-4237-8640-5719ddb050c2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoPro/API/Account/PostUsers/API_ACNT_When_new_fieldcrew_account_is_created_Expected_new_account_is_added_in_db_with_correct_roleid</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f4f84aaa-e067-47fc-89a0-f7b22163aedd</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoPro/API/Account/PostUsers/API_ACNT_When_new_account_have_all_roles_attribute_Expected_status_code_200</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a9c6d034-90a3-44c3-b07c-69f9fb0a59e8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoPro/API/Account/PostUsers/API_ACNT_When_new_account_is_created_with_all_roles_Expected_new_account_is_added_in_db_with_correct_roleids</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2dcdf64a-f8bd-498e-9f89-199651837369</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoPro/API/Account/PostUsers/API_ACNT_When_client_account_try_to_create_a_new_account_Expected_status_code_401</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d967b79f-02cb-4167-9143-08bf3dd3a2af</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoPro/API/Account/PostUsers/API_ACNT_When_agent_account_try_to_create_a_new_account_Expected_status_code_401</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>805ca916-150e-4de2-bf44-f28c0061aa0f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoPro/API/Account/PostUsers/API_ACNT_When_fieldcrew_account_try_to_create_a_new_account_Expected_status_code_401</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
