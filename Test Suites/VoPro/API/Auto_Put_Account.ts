<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Auto_Put_Account</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>e34f66a2-9ee6-4e5b-9012-a7f48544ac48</testSuiteGuid>
   <testCaseLink>
      <guid>ec13df9d-e57c-4676-b0b2-1fca04e6cda3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoPro/API/Account/PutUsers/API_ACNT_When_account_is_updated_Expected_new_account_is_updated_in_database</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>3c398fa9-83a3-4418-bd63-34bfd600a93d</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>ee1be7f5-9d06-4192-ba69-eae1028c22b2</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>8e4d9cce-4116-4ecc-a89e-545475c02801</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>78cfea44-285b-4e79-b6a2-88bc8286bf40</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>ad3d14b6-4d0f-4f6c-aa27-5e6fe775f29a</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>c7d510c9-cb92-4487-b2af-7a33050dcc90</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>c845a3cd-ef1f-4602-9239-d7cf113984aa</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>cd3ef6af-7a4b-4a8e-9940-e315a28fa846</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>3f2e7ee6-4b38-4edb-98f2-83e77bcb8a4b</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>addec434-6fd4-4617-82a3-64a6719bcc16</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>26c0564c-ede9-4d59-88aa-3a45abcc6aed</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>68039b02-847c-4e4d-9dbb-2a744ae66c75</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>49c27aad-c7c1-4f87-a11f-3b6ad2b170e1</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>fc9e1014-212b-4068-bda3-0456a4798695</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>05010237-bcf4-480b-97cb-0506aa276657</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>179bcc62-8537-4843-9ed4-2e3b5c64d4e7</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>1ebd555b-450d-456a-ab16-48f0826b7b68</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoPro/API/Account/PutUsers/API_ACNT_When_account_is_updated_Expected_status_code_200</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>4162ebc5-17cc-49c1-87b1-3dd737263ae9</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>ec0909ec-a45b-49ef-9c99-234d67617c76</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>cc7b76cf-eccc-48c6-a08d-211c1cd18c6d</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>2e97c372-8f47-4850-b495-a294d612ff0e</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>39603a67-a6e2-4ebc-8a16-b0f20097cc03</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>77542bb6-42f6-45f9-b7d6-c9018010b530</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>00516785-8de3-405d-a375-cf521a29158a</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>29d20c1a-3ad3-4cba-8e3c-b55e8c807106</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>8e34bb4e-910d-4472-b7db-eecb0857ce58</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>49adabb7-53e0-41fc-be47-2deffb49e881</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>8e4da7fb-f93a-40ec-9529-56109bd4d20c</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>aaad4922-a5a2-4a8d-82b2-3d398e9fc231</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>14fb436e-d60b-41a6-8517-06fd1b37b255</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>aa4cb52e-9ea8-453b-ac99-506dbee96bd7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoPro/API/Account/PutUsers/API_ACNT_When_account_updated_have_less_6_characters_in_password_attribute_Expected_status_code_400</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f8b1abfa-ac47-4788-8575-47f641fca7e0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoPro/API/Account/PutUsers/API_ACNT_When_account_updated_have_more_15_characters_in_password_attribute_Expected_status_code_400</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ac25d76a-1496-45f3-95cb-05015d400f46</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoPro/API/Account/PutUsers/API_ACNT_When_account_updated_have_15_characters_in_password_attribute_Expected_status_code_200</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>40cb8475-e5cb-4833-b5f4-227192eac9a6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoPro/API/Account/PutUsers/API_ACNT_When_account_updated_have_spaces_in_password_attribute_Expected_status_code_400</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f461bfbc-9098-45d4-9b98-50024faef10d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoPro/API/Account/PutUsers/API_ACNT_When_account_updated_have_only_spaces_in_password_attribute_Expected_status_code_200</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>168cb9a0-2074-4542-ae46-29ab86be730f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoPro/API/Account/PutUsers/API_ACNT_When_account_updated_has_symbols_in_password_attribute_Expected_status_code_200</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>773098b5-6507-4671-b624-0bc2cab1d0ab</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoPro/API/Account/PutUsers/API_ACNT_When_account_updated_with_6_characters_in_password_attribute_Expected_status_code_200</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5a9da4bb-51f6-49d6-9d28-089085a8b470</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoPro/API/Account/PutUsers/API_ACNT_When_account_updated_has_an_existing_email_value_in_email_attribute_Expected_status_code_400</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>252492f4-a8e6-465d-856e-30718beb17d5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoPro/API/Account/PutUsers/API_ACNT_When_new_account_have_wrong_format_in_email_attribute_Expected_status_code_200</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>96eacf4f-560e-4c9f-9183-065419b02a87</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoPro/API/Account/PutUsers/API_ACNT_When_account_updated_has_spaces_in_email_attribute_Expected_status_code_200</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7d4390e4-e77a-41f1-8e90-5ed98713c194</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoPro/API/Account/PutUsers/API_ACNT_When_account_updated_has_symbols_in_email_attribute_Expected_status_code_400</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8e5ed523-0fb3-4b02-8e3c-56eb4c8e5c5b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoPro/API/Account/PutUsers/API_ACNT_When_account_updated_have_long_fullname_attribute_Expected_status_code_200</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>242aadc9-d9b8-42d1-8378-cb11ad6792ea</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoPro/API/Account/PutUsers/API_ACNT_When_account_updated_have_symbols_in_phone_attribute_Expected_status_code_200</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>79825e4b-554f-4f54-8066-f98fe0c17153</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoPro/API/Account/PutUsers/API_ACNT_When_account_updated_have_long_dealer_attribute_Expected_status_code_200</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>fc17420b-aab7-4320-8b36-2f2204cc3317</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoPro/API/Account/PutUsers/API_ACNT_When_account_updated_have_symbols_in_dealer_attribute_Expected_status_code_200</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>414398e6-3aa1-4f4e-a69e-09dbeaadf899</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoPro/API/Account/PutUsers/API_ACNT_When_account_updated_have_long_addressline1_attribute_Expected_status_code_200</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4c3bfb00-2dfc-43d1-b05b-eb685c80e680</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoPro/API/Account/PutUsers/API_ACNT_When_account_updated_have_symbols_in_addressline1_attribute_Expected_status_code_200</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>59904f54-037f-46bb-b446-0e0144183a54</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoPro/API/Account/PutUsers/API_ACNT_When_account_updated_have_long_addressline2_attribute_Expected_status_code_200</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>95af553e-d68d-4edd-b0e2-168e761c88a2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoPro/API/Account/PutUsers/API_ACNT_When_account_updated_have_symbols_in_addressline2_attribute_Expected_status_code_200</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>df127eae-a5a5-4a22-9001-0e5a16d94325</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoPro/API/Account/PutUsers/API_ACNT_When_account_updated_have_long_town_attribute_Expected_status_code_200</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5ee67c7a-fd79-47b0-b48f-8f4f6a6e0405</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoPro/API/Account/PutUsers/API_ACNT_When_account_updated_have_symbols_in_town_attribute_Expected_status_code_200</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8c0eeb5a-9240-45bc-8873-68ddd12c646a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoPro/API/Account/PutUsers/API_ACNT_When_account_updated_have_long_state_attribute_Expected_status_code_200</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>cdc7a7ab-9722-4bb8-8d3f-d793a4c43e42</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoPro/API/Account/PutUsers/API_ACNT_When_account_updated_have_spaces_in_state_attribute_Expected_status_code_200</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>967b38a2-fece-4a62-9bda-8dd382c00801</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoPro/API/Account/PutUsers/API_ACNT_When_account_updated_have_symbols_in_state_attribute_Expected_status_code_200</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>fb0f2324-d6a4-46e5-a474-00c64a1b0b8f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoPro/API/Account/PutUsers/API_ACNT_When_account_updated_have_long_zip_attribute_Expected_status_code_200</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f4ec00eb-07e1-4710-ac58-d86303cc8de0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoPro/API/Account/PutUsers/API_ACNT_When_account_updated_have_chars_zip_attribute_Expected_status_code_200</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>95680d5c-783c-4e2b-ac20-3bccf69eb25f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoPro/API/Account/PutUsers/API_ACNT_When_account_updated_have_spaces_in_zip_attribute_Expected_status_code_200</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>09283230-58e9-4d61-b2e3-ff9af792544b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoPro/API/Account/PutUsers/API_ACNT_When_account_updated_have_symbols_in_zip_attribute_Expected_status_code_200</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>81f7d7cb-e461-496a-bebd-0d9dce575de2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoPro/API/Account/PutUsers/API_ACNT_When_account_updated_have_number_in_zip_attribute_Expected_status_code_200</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>271b4f4e-dd97-42b4-82f2-0d558776a179</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoPro/API/Account/PutUsers/API_ACNT_When_account_updated_have_long_country_attribute_Expected_status_code_200</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4466a37e-4373-491a-b410-b5791ccb6e2e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoPro/API/Account/PutUsers/API_ACNT_When_account_updated_have_number_country_attribute_Expected_status_code_200</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2955fc5b-ade3-45b5-9640-55391effe590</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoPro/API/Account/PutUsers/API_ACNT_When_account_updated_have_spaces_in_country_attribute_Expected_status_code_200</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>04766ac4-80fd-469b-93ca-c0b88b973059</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoPro/API/Account/PutUsers/API_ACNT_When_account_updated_have_symbols_in_country_attribute_Expected_status_code_200</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>897d9014-aa90-49b9-b7b3-d3b3f07e348b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoPro/API/Account/PutUsers/API_ACNT_When_account_updated_have_invalid_roles_attribute_Expected_status_code_400</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0b5874d3-aafe-4a73-b198-3a78fe33986a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoPro/API/Account/PutUsers/API_ACNT_When_account_updated_have_number_in_roles_attribute_Expected_status_code_400</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>de24aac7-cb93-4b7f-ad55-1799d894ee0f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoPro/API/Account/PutUsers/API_ACNT_When_account_updated_have_spaces_in_roles_attribute_Expected_status_code_200</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>880c2b9e-fd3e-488e-bd8e-b5ece63d5fb2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoPro/API/Account/PutUsers/API_ACNT_When_account_updated_have_symbols_in_roles_attribute_Expected_status_code_400</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>81aeb37c-b109-4741-a6cb-c1faa5ec1045</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoPro/API/Account/PutUsers/API_ACNT_When_account_updated_have_roles_attribute_as_client_Expected_status_code_200</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>38643d66-c8f1-4427-9350-b71732b8563c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoPro/API/Account/PutUsers/API_ACNT_When_role_account_is_updated_as_client_Expected_account_is_updated_in_db_with_correct_roleid</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>abf86478-cc69-4d6a-bb53-3fc8190d82fc</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoPro/API/Account/PutUsers/API_ACNT_When_account_updated_has_roles_attribute_as_administrator_Expected_status_code_200</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6e501548-798c-4205-95e4-ad1494c94848</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoPro/API/Account/PutUsers/API_ACNT_When_role_account_is_updated_as_administrator_Expected_account_is_updated_in_db_with_correct_roleid</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9df00f58-a668-4009-b5ea-c8516f26d9e5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoPro/API/Account/PutUsers/API_ACNT_When_account_updated_has_roles_attribute_as_agent_Expected_status_code_200</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c4b43261-8686-438f-a896-3da592dc7798</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoPro/API/Account/PutUsers/API_ACNT_When_account_is_updated_as_agent_a_Expected_account_is_updated_in_db_with_correct_roleid</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>66eeb8eb-e944-4866-b52f-2b68b447ad0e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoPro/API/Account/PutUsers/API_ACNT_When_account_updated_has_roles_attribute_as_fieldcrew_Expected_status_code_200</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>464cb030-5b09-4823-b5a3-41e09c493fcc</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoPro/API/Account/PutUsers/API_ACNT_When_account_is_updated_as_fieldcrew_Expected_account_is_updated_in_db_with_correct_roleid</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>45d37b84-1254-476c-8de0-5040c58896da</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoPro/API/Account/PutUsers/API_ACNT_When_account_updated_has_all_roles_attribute_Expected_status_code_200</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>247fbd0a-8139-4704-a237-46aef45b3ba2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoPro/API/Account/PutUsers/API_ACNT_When_account_is_updated_with_all_roles_Expected_account_is_updated_in_db_with_correct_roleids</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>134e2f3f-038e-4d5e-9eda-ddaf47749d0b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoPro/API/Account/PutUsers/API_ACNT_When_client_account_try_to_create_a_new_account_Expected_status_code_401</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1d91520c-9272-4e7c-acf4-50f48eb6f34f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoPro/API/Account/PutUsers/API_ACNT_When_agent_account_try_to_create_a_new_account_Expected_status_code_401</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0682b184-3b86-4fcd-a312-003d238e8e25</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoPro/API/Account/PutUsers/API_ACNT_When_fieldcrew_account_try_to_create_a_new_account_Expected_status_code_401</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>38fab75d-fb70-4c27-a548-bb4c2b59ae8a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoPro/API/Account/PutUsers/API_ACNT_When_account_updated_without_addressline1_attribute_Expected_status_code_200</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>30a4536e-cf45-4c0f-a275-1ae4e4026d41</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoPro/API/Account/PutUsers/API_ACNT_When_account_updated_without_addressline2_attribute_Expected_status_code_200</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>470038c7-d9e8-42c6-bd98-1af52108b751</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoPro/API/Account/PutUsers/API_ACNT_When_account_updated_without_country_attribute_Expected_status_code_200</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>57b3fa37-ff47-4c54-b628-4a3de64732a9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoPro/API/Account/PutUsers/API_ACNT_When_account_updated_without_dealer_attribute_Expected_status_code_200</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>300f25cb-94ef-403e-bed5-665c6f14be8d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoPro/API/Account/PutUsers/API_ACNT_When_account_updated_without_email_attribute_Expected_status_code_400</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4be21230-b9ca-479a-8c6c-9a372de8f114</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoPro/API/Account/PutUsers/API_ACNT_When_account_updated_without_fullname_attribute_Expected_status_code_400</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9080c6fe-acde-402c-8e1c-f2bef8aef22c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoPro/API/Account/PutUsers/API_ACNT_When_account_updated_without_password_attribute_Expected_status_code_200</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e9b4585a-a3d3-4c92-b157-5520a1da9169</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoPro/API/Account/PutUsers/API_ACNT_When_account_updated_without_phone_attribute_Expected_status_code_200</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>12232533-0ec5-42ab-886a-ffd4d7c701eb</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoPro/API/Account/PutUsers/API_ACNT_When_account_updated_without_roles_attribute_Expected_status_code_400</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a7ce9d9a-4211-47ac-8469-843c279de82e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoPro/API/Account/PutUsers/API_ACNT_When_account_updated_without_state_attribute_Expected_status_code_200</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>acb2e403-a11c-481e-89fb-0438e41a42a4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoPro/API/Account/PutUsers/API_ACNT_When_account_updated_without_town_attribute_Expected_status_code_200</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>feef0b36-54b4-434b-9415-be274275f52b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoPro/API/Account/PutUsers/API_ACNT_When_account_updated_without_zip_attribute_Expected_status_code_200</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6fee0934-132e-4abd-9a7a-f28998493aa8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoPro/API/Account/PutUsers/API_ACNT_When_account_updated_with_phone_attribute_as_char_Expected_status_code_200</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>12b88b89-140d-41ef-b8b9-ec6e1e155f78</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoPro/API/Account/PutUsers/API_ACNT_When_account_updated_with_phone_attribute_as_spaces_Expected_status_code_200</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
