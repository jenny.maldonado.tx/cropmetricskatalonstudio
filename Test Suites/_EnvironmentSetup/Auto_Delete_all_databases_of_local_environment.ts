<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Auto_Delete_all_databases_of_local_environment</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>eb4adb3a-b586-48a3-8795-15cf4d8a42d2</testSuiteGuid>
   <testCaseLink>
      <guid>076b943f-ece5-4b75-9d6a-b7f16cc66403</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Databases/DeleteDatabases/Functional/DB_Delete_iterisrawdata_database</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6e148636-426e-49dd-b151-8937c502ce77</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Databases/DeleteDatabases/Functional/DB_Delete_virtualoptimizer_database</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>84687cb3-2374-448c-9f7e-0585bc63fbf9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Databases/DeleteDatabases/Functional/DB_Delete_vo_probereadings_database</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e8e5df14-5a97-429d-86aa-6d3ea292fa5e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Databases/DeleteDatabases/Functional/DB_Delete_vo_summarizeddata_database</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>678dd1a4-b9b8-441d-96be-c561c47c0089</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Databases/DeleteDatabases/Functional/DB_Delete_vo_ssurgoRawData_database</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>cf16cfb3-8516-4a9e-9fd1-dacd9fd38c5f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Databases/DeleteDatabases/Functional/DB_Delete_vo_fieldsProcessed_database</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>09402a5a-621e-47e5-b30b-c250f68e1f7d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Databases/DeleteDatabases/ProbeReadings/DB_Delete_vo_agsenseproberawreadings_database</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>13a55767-860d-4df0-9580-2ec614759d3b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Databases/DeleteDatabases/ProbeReadings/DB_Delete_vo_agsenseprobereadings_database</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c1c0c066-8a8f-4196-ad8b-45174564f7cd</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Databases/DeleteDatabases/ProbeReadings/DB_Delete_vo_loraproberawreadings_database</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>315e429b-4734-4cb0-bc3a-63f0303af18e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Databases/DeleteDatabases/ProbeReadings/DB_Delete_vo_sentekproberawreadings_database</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ed268884-44ec-42e5-87e0-a07e5c7ac95b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Databases/DeleteDatabases/ProbeReadings/DB_Delete_vo_sentekprobereadings_database</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
