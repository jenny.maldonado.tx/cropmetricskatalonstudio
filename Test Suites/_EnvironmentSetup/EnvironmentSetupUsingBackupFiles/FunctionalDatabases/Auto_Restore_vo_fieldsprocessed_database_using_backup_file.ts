<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Auto_Restore_vo_fieldsprocessed_database_using_backup_file</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>528f7fb8-2274-4436-a583-d04ace4e1626</testSuiteGuid>
   <testCaseLink>
      <guid>b4b2ef0b-0867-40d0-a8fb-1f298573eaf8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Databases/CreateDatabases/Functional/DB_Create_vo_fieldsProcessed_database</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>010d1ff9-ac56-4a51-a259-6fcf29f7e79b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Databases/DatabasesRestoreWithBakcupFiles/Functional/DB_Restore_vo_fieldsProcessed_with_backup_file</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
