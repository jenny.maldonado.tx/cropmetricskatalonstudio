<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Auto_Restore_vo_ssurgoRawData_database_using_backup_file</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>d0f18219-0af4-4cc7-82d5-d0ae14d87e40</testSuiteGuid>
   <testCaseLink>
      <guid>cec043c4-2f93-448d-9b0f-99df501ede93</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Databases/CreateDatabases/Functional/DB_Create_vo_ssurgoRawData_database</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f72e3a4c-5727-46d3-b394-6b3f2d4734bb</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Databases/DatabasesRestoreWithBakcupFiles/Functional/DB_Restore_vo_ssurgoRawData_with_backup_file</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
