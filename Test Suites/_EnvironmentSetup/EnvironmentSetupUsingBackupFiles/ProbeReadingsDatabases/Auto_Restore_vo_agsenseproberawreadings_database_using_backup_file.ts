<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Auto_Restore_vo_agsenseproberawreadings_database_using_backup_file</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>16c97150-6b18-4d8e-808f-636b7d4c9b3b</testSuiteGuid>
   <testCaseLink>
      <guid>2a51169b-dc95-468a-ab09-2633637e0666</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Databases/CreateDatabases/ProbeReadings/DB_Create_vo_agsenseproberawreadings_database</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>621f962a-0279-42f0-bb49-9a798741c7c6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Databases/DatabasesRestoreWithBakcupFiles/ProbeReadings/DB_Restore_vo_agsenseproberawreadings_with_backup_file</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
