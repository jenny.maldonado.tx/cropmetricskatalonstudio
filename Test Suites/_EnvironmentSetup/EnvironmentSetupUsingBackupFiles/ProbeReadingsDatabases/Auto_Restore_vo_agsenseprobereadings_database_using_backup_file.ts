<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Auto_Restore_vo_agsenseprobereadings_database_using_backup_file</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>4f73f56b-bcab-4755-b2b1-2e168d7daa32</testSuiteGuid>
   <testCaseLink>
      <guid>1cc0d16c-147d-40aa-82da-d8eabab2e9d9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Databases/CreateDatabases/ProbeReadings/DB_Create_vo_agsenseprobereadings_database</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>80cdea2d-0ed7-4712-b542-a9dcdd23a899</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Databases/DatabasesRestoreWithBakcupFiles/ProbeReadings/DB_Restore_vo_agsenseprobereadings_with_backup_file</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
