<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Auto_Restore_vo_sentekproberawreadings_database_using_backup_file</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>25f696b7-beb9-4ae6-82fe-f23a7aebf472</testSuiteGuid>
   <testCaseLink>
      <guid>4ae063a4-b8d6-4610-8107-f71b789ebefc</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Databases/CreateDatabases/ProbeReadings/DB_Create_vo_sentekproberawreadings_database</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1dbfd18f-38b5-4316-8f26-a8a694dc2e63</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Databases/DatabasesRestoreWithBakcupFiles/ProbeReadings/DB_Restore_vo_sentekproberawreadings_wiht_backup_file</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
