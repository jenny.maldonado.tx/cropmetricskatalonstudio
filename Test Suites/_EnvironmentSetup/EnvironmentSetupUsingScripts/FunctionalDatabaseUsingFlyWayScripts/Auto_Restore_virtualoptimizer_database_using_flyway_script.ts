<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Auto_Restore_virtualoptimizer_database_using_flyway_script</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>038f27f3-d1ef-48fb-919b-ddc9e0439dcb</testSuiteGuid>
   <testCaseLink>
      <guid>4dc8c343-97a2-407f-8507-87479ebb7c28</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Databases/CreateDatabases/Functional/DB_Create_virtualoptimizer_database</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>977294e8-1132-4863-8124-e5af56d862b8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Databases/DatabasesWithFlyway/Functional/DB_virtualoptimizer_wiht_flyway</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
