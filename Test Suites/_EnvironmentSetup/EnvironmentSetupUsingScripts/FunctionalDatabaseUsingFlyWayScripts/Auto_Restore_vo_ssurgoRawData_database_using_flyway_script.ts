<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Auto_Restore_vo_ssurgoRawData_database_using_flyway_script</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>4ef56115-14ab-4cbd-aa48-aabeab9ee44f</testSuiteGuid>
   <testCaseLink>
      <guid>214b6be4-e71d-4a47-81eb-6fe97cc0c94c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Databases/CreateDatabases/Functional/DB_Create_vo_ssurgoRawData_database</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c675eafe-3dc5-4595-94f6-04a9c6e6fba4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Databases/DatabasesWithFlyway/Functional/DB_vo_ssurgoRawData_with_fkyway</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
