<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Auto_Restore_vo_summarizeddata_database_using_flyway_script</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>cbbcfa8c-e720-4646-9d05-0c179b711034</testSuiteGuid>
   <testCaseLink>
      <guid>0fda7d2d-7c92-4eda-9d96-845fa3a6620f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Databases/CreateDatabases/Functional/DB_Create_vo_summarizeddata_database</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>791b240a-8543-44d9-aab9-dd893ce61e1e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Databases/DatabasesWithFlyway/Functional/DB_vo_summarizeddata_with_fkyway</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
