<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteCollectionEntity>
   <description></description>
   <name>Auto_Restore_all_probe_readings_databases_using_sql_script</name>
   <tag></tag>
   <executionMode>SEQUENTIAL</executionMode>
   <maxConcurrentInstances>8</maxConcurrentInstances>
   <testSuiteRunConfigurations>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/_EnvironmentSetup/EnvironmentSetupUsingScripts/ProbeReadingsDatabaseUsingSQLScripts/Auto_Restore_vo_agsenseproberawreadings_database_using_sql_script</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/_EnvironmentSetup/EnvironmentSetupUsingScripts/ProbeReadingsDatabaseUsingSQLScripts/Auto_Restore_vo_sentekproberawreadings_database_using_sql_script</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/_EnvironmentSetup/EnvironmentSetupUsingScripts/ProbeReadingsDatabaseUsingSQLScripts/Auto_Restore_vo_agsenseprobereadings_database_using_sql_script</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/_EnvironmentSetup/EnvironmentSetupUsingScripts/ProbeReadingsDatabaseUsingSQLScripts/Auto_Restore_vo_sentekprobereadings_database_using_sql_script</testSuiteEntity>
      </TestSuiteRunConfiguration>
   </testSuiteRunConfigurations>
</TestSuiteCollectionEntity>
