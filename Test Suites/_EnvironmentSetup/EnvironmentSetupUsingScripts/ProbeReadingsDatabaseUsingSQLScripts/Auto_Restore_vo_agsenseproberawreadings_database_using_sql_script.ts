<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Auto_Restore_vo_agsenseproberawreadings_database_using_sql_script</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>7a95cb18-38b3-4578-9ec5-70ff7dc01838</testSuiteGuid>
   <testCaseLink>
      <guid>b413dc26-ab70-4f66-83b9-45d3a14de925</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Databases/CreateDatabases/ProbeReadings/DB_Create_vo_agsenseproberawreadings_database</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3d88d6b5-6b65-40ca-bfe8-0c664a2ece80</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Databases/DatabasesWithFlyway/ProbeReadings/DB_vo_agsenseproberawreadings_with_flyway</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c1b6c994-d4c7-4e1b-ba57-f1ff248ae5c1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Databases/ProbeReadingsRestoreWithSqlScripts/DB_vo_agsenseproberawreadings_with_sql_script_for_schema</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d68fcce9-0420-4370-9fe3-6c2a386b52dd</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Databases/ProbeReadingsRestoreWithSqlScripts/DB_vo_agsenseproberawreadings_with_sql_script_for_data</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
