<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Auto_Restore_vo_agsenseprobereadings_database_using_sql_script</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>c0d115cc-f4a1-4a27-a9c4-436a94adc86e</testSuiteGuid>
   <testCaseLink>
      <guid>ae11e4aa-e1d9-454b-b1e5-c17313d2137a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Databases/CreateDatabases/ProbeReadings/DB_Create_vo_agsenseprobereadings_database</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>447b12b4-b5d3-4f12-89c9-4c63f653e72f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Databases/DatabasesWithFlyway/ProbeReadings/DB_vo_agsensesprobereadings_with_flyway</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>57f8847a-a07e-401b-9b20-b08431836298</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Databases/ProbeReadingsRestoreWithSqlScripts/DB_vo_agsenseprobereadings_with_sql_script_for_schema</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1013fd5b-023c-4b0f-8bf8-6f8c4fb8281f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Databases/ProbeReadingsRestoreWithSqlScripts/DB_vo_agsenseprobereadings_with_sql_script_for_data</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
